import category.AdministrativeSmokeTests;
import login.LoginAdministrate;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


/**
 * Created by Kraynov-SA on 07.09.2018.
 */

@RunWith(Categories.class)
@Categories.IncludeCategory(AdministrativeSmokeTests.class)
@Suite.SuiteClasses(LoginAdministrate.class)

public class RunLoginAdministrativeSmokeTests {
}
