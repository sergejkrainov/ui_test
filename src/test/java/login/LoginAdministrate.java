package login;

import category.AdministrativeSmokeTests;
import io.qameta.allure.Feature;
import libs.WebDriverUtil;
import libs.alm.AlmTest;
import libs.alm.AlmTestRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import pages.LoginAdministrative;
import testbase.UiTestBase;
import config.Config;

/**
 * Выполняем логин в приклад ACAdministrate.
 */
public class LoginAdministrate extends UiTestBase {

    @Rule
    public AlmTestRule rule = new AlmTestRule();

    @Test
    @Feature(value = "Тест LoginAdministrative")
    @Category({AdministrativeSmokeTests.class})
    @AlmTest(testId = "36975", groupId = "9110", domain = "PPRB", project = "PPRB_BDSP_Deposits")
    public final void test() throws Exception {
        new LoginAdministrative().login(Config.getConf().get("acAdministrateUrl"), Config.getConf().get("iftUsername"),
                Config.getConf().get("iftPassword"));

        WebDriverUtil.checkElementPresent("//img[@src='/moncod2-web/css/base/images/LogoSBRF.gif']");
        WebDriverUtil.checkPageContains("Редактор вкладов");
    }

    @Test
    @Feature(value = "Тест LoginAdministrative")
    @Category({AdministrativeSmokeTests.class})
    @AlmTest(testId = "36975", groupId = "9110", domain = "PPRB", project = "PPRB_BDSP_Deposits")
    public final void test2() throws Exception {
        new LoginAdministrative().login(Config.getConf().get("acAdministrateUrl"), Config.getConf().get("iftUsername"),
                Config.getConf().get("iftPassword"));

        WebDriverUtil.checkElementPresent("//img[@src='/moncod2-web/css/base/images/LogoSBRF.gif']");
        WebDriverUtil.checkPageContains("Редактор вкладов");
    }
}
