package login;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true, plugin = {"pretty", "ru.yandex.qatools.allure.cucumberjvm.AllureReporter"},
        glue = {"stepdefs"},
        features = {"src/test/resources/features/"},
        tags = {"@LoginAdministrate"}
)
public class CucumberTest {
}
