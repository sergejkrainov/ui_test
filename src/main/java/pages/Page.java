package pages;

import config.Shared;
import libs.DelegateWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Page {
    protected static final Logger LOG = LoggerFactory.getLogger(Page.class);
    private DelegateWebDriver webDriver = Shared.getDelegateWebDriver();

    public Page() {
        PageFactory.initElements(webDriver, this);
    }

    public final DelegateWebDriver getDelegateWebDriver() {
        return  this.webDriver;
    }
}
