package pages;

import static libs.WebDriverUtil.click;

import libs.DelegateWebDriver;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ChooseUser extends Page {
    @FindBy(xpath = "//table[@class='tablebg']")
    private static WebElement firstTable;

    @FindBy(id = "tableForm:dataTable")
    private static WebElement secondTable;

    @FindBy(className = "dataTableC")
    private static WebElement thirdTable;

    private static ArrayList<WebElement> tables = new ArrayList<>();

    private DelegateWebDriver webDriver = getDelegateWebDriver();

    private final int timeoutLong = 30;

    private final int timeoutFast = 1;

    public ChooseUser() {
        tables.clear();
        tables.add(firstTable);
        tables.add(secondTable);
        tables.add(thirdTable);
    }

    /**
     *
     * @param column - имя столбца.
     * @param value - значение.
     * @return - страница с выбором.
     * @throws TimeoutException - класс с ошибкой.
     */
    public final ChooseUser chooseUser(String column, String value) throws TimeoutException {
        LOG.info("Выбор пользователя из таблицы");
        final long timeout = 60;
        long timePassed = 0;

        while (true) {
            try {
                String pageSource = webDriver.getPageSource();
                if (pageSource.contains(value)) {
                    break;
                }
            } catch (Exception e) {
                LOG.error("Ошибка:" + e.getMessage());
            }
            if (timePassed > timeout) {
                LOG.error("ОШИБКА: Не открылась страница выбора пользователя.");
                LOG.debug("Код открытой страницы: " + webDriver.getPageSource());
                throw new TimeoutException("Не открылась страница выбора пользователя");
            }
            webDriver.manage().timeouts().implicitlyWait(timeoutFast, TimeUnit.SECONDS);
            timePassed += 1;


        }

        for (WebElement table : ChooseUser.tables) {
            try {
                if (table.isDisplayed()) {
                    Table.setRootElement(table);
                    clickEnter(column, value, "Войти");
                }
            } catch (NoSuchElementException e) {
                LOG.debug("Одна из таблиц не подошла");
            }
        }

        webDriver.manage().timeouts().implicitlyWait(timeoutLong, TimeUnit.SECONDS);

        return new ChooseUser();
    }

    private void clickEnter(String column, String value, String desiredColumn) {
        click(Table.getCell(column, value, desiredColumn));
    }
}
