package pages;

import static libs.WebDriverUtil.open;
import static libs.WebDriverUtil.sendKeys;
import static libs.WebDriverUtil.click;

import org.openqa.selenium.WebElement;

public abstract class LoginPage<T extends Page> extends Page {
    public abstract WebElement getUsername();

    public abstract WebElement getPassword();

    public abstract WebElement getEnterButton();

    public abstract T getNextPage();

    /**
     *
     * @param url - url страницы.
     * @param username - имя пользователя.
     * @param password - пароль.
     * @return - страница.
     */
    public final T login(String url, String username, String password) throws IllegalAccessException, ClassNotFoundException {
        LOG.info("Открытие страницы входа");

        open(url);

        LOG.info("Ввод данных пользователя");

        sendKeys(getUsername(), username);
        sendKeys(getPassword(), password);

        LoginAdministrative.getElementsTextProperties(this);

        click(getEnterButton());
        return getNextPage();
    }
}
