package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public final class Table {

    private Table(){

    }

    private static By byLocator;
    private static final Logger LOG = LoggerFactory.getLogger(Table.class);
    private static WebElement rootElement;
    private static List<WebElement> tableHeaders = new ArrayList<>();

    public static void setRootElement(WebElement webElement) {
        rootElement = webElement;
    }

    /**
     *
     * @return - список элементов заголовков таблицы.
     */
    public static List<WebElement> getTableHeaders() {
        LOG.info("Получение списка элементов заголовков таблицы");

        if (tableHeaders.isEmpty()) {
            tableHeaders = rootElement.findElements(By.xpath(".//thead//tr//th"));
        }
        return tableHeaders;
    }

    public static WebElement getHeaderByName(String headerName) {
        LOG.info("Получение заголовка с названием \"" + headerName + "\"");
        return findElement(By.xpath("./thead/tr//*[contains(text(), '" + headerName + "')]"));
    }

    public static WebElement getCell(int rownum, int colnum) {
        LOG.info("Получение ячейки, строка \"" + rownum + "\", столбец \"" + colnum + "\"");
        return findElement(By.xpath("tbody/tr[" + rownum + "]/td[count(../../../thead/tr/th[" + colnum + "]/preceding-sibling::*) + 1]"));
    }

    public static WebElement getCell(int rownum, String columnName) {
        LOG.info("Получение ячейки, строка \"" + rownum + "\", столбец \"" + columnName + "\"");
        return findElement(By.xpath(".//tbody/tr[" + (rownum + 1) + "]/td[count(//thead/tr/th[.='" + columnName + "']/preceding-sibling::*) + 1]"));
    }

    /**
     *
     * @param columnName - имя столбца.
     * @param value - значение.
     * @return - список элементов.
     */
    public static WebElement getCell(String columnName, String value) {
        LOG.info("Получение ячейки, столбец \"" + columnName + "\", значение \"" + value + "\"");
        return findElement(By.xpath("tbody/tr/td[count(../../../thead/tr/th[.='" + columnName + "']/preceding-sibling::*) + 1 and contains(text(), '"
                + value + "')]|"
                + "tbody/tr/td[count(../../../thead/tr/th[.='" + columnName + "']/preceding-sibling::*) + 1]/*[contains(text(), '" + value + "')]"));
    }

    /**
     *
     * @param columnName - имя столбца.
     * @param value - значение.
     * @param desiredColumn - описание.
     * @return - список элементов.
     */
    public static WebElement getCell(String columnName, String value, String desiredColumn) {
        LOG.info("Получение ячейки столбца \"" + desiredColumn + "\", по столбцу \"" + columnName + "\" и значению \"" + value + "\"");
        return findElement(By.xpath("tbody/tr/td[count(../../../thead/tr/th[.='" + columnName + "']/preceding-sibling::*) + 1 and contains(text(), '"
                + value + "')]/../*[count(../../../thead/tr/th[.='" + desiredColumn + "']/preceding-sibling::*) + 1]|"
                + ".//tbody/tr/td[count(//thead/tr/th[.='" + columnName + "']/preceding-sibling::*) + 1]/*[contains(text(), '" + value
                + "')]/../../td[count(//thead/tr/th[.='" + desiredColumn + "']/preceding-sibling::*) + 1]"));
    }

    public static List<WebElement> getCells(String columnName) {
        LOG.info("Получение ячеек, столбца \"" + columnName + "\"");
        return findElements(By.xpath(".//tbody/tr/td[count(//thead/tr/th[.='" + columnName + "']/preceding-sibling::*) + 1]"));
    }

    /**
     * @param columnName - имя столбца.
     * @param value - значение.
     * @param desiredColumn - описание.
     * @return - список элементов.
     */
    public static List<WebElement> getCells(String columnName, String value, String desiredColumn) {
        LOG.info("Получение ячеек столбца \"" + desiredColumn + "\", по столбцу \"" + columnName + "\" и значению \"" + value + "\"");
        return findElements(By.xpath("tbody/tr/td[count(../../../thead/tr/th[.='" + columnName + "']/preceding-sibling::*) + 1 and contains(text(), '"
                + value + "')]/../*[count(../../../thead/tr/th[.='" + desiredColumn + "']/preceding-sibling::*) + 1]|"
                + ".//tbody/tr/td[count(//thead/tr/th[.='" + columnName + "']/preceding-sibling::*) + 1]/*[contains(text(), '" + value
                + "')]/../../td[count(//thead/tr/th[.='" + desiredColumn + "']/preceding-sibling::*) + 1]"));
    }

    public static List<WebElement> getRows() {
        LOG.info("Получение строк таблицы");
        return findElements(By.xpath("tbody/tr"));
    }

    public static boolean isEmpty() {
        return findElements(By.xpath("tbody/tr")).size() == 0;
    }

    private static WebElement findElement(By by) {
        return rootElement.findElement(by);
    }

    private static List<WebElement> findElements(By by) {
        return rootElement.findElements(by);
    }
}
