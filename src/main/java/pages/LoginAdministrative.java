package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Properties;

public class LoginAdministrative extends LoginPage<ChooseUser> {
    @FindBy(name = "j_username")
    private static WebElement username;

    @FindBy(name = "j_password")
    private static WebElement password;

    @FindBy(css = "input[value='Войти']")
    private static WebElement enterButton;

    @FindBy(className = "dialogTitle")
    private static WebElement dialogTitle;

    public final WebElement getUsername() {
        return username;
    }

    public final WebElement getPassword() {
        return password;
    }

    public final WebElement getEnterButton() {
        return enterButton;
    }

    public final ChooseUser getNextPage() {
        return new ChooseUser();
    }

    /**
     *
     * @param obj - Page object.
     * @return - text properties.
     * @throws ClassNotFoundException - ClassNotFoundException.
     * @throws IllegalAccessException - IllegalAccessException.
     */
    public static Properties getElementsTextProperties(Object obj) throws ClassNotFoundException, IllegalAccessException {

        Properties actualProps = new Properties();

        Class<?> cl = obj.getClass();

        Field[] allFields = cl.getDeclaredFields();

        for (Field curFl: allFields) {

            if (curFl.getType().getName().equals("org.openqa.selenium.WebElement")) {
                WebElement cur = (WebElement) curFl.get(obj);
                actualProps.setProperty(curFl.getName(), cur.getText());
            }
        }

        for (Map.Entry<Object, Object> st: actualProps.entrySet()) {
            System.out.println("Key = " + st.getKey() + " ; Value = " + st.getValue());
        }
        return actualProps;
    }
}
