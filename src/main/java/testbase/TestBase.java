package testbase;

import libs.TestErrorCollector;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author out-tsaplin-ad.
 */
public abstract class TestBase {

    @ClassRule
    public static TestErrorCollector ec = new TestErrorCollector();

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Before
    public final void initTestBase() throws Exception {
        log.info("Инициализация теста");
    }

    @After
    public final void afterTestBase() {
        log.info("Завершение теста");
    }

    public final Logger getLogger() {
        return log;
    }
}
