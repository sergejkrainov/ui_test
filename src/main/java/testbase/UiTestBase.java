package testbase;

import config.Shared;
import libs.DelegateWebDriver;
import libs.Init;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UiTestBase extends TestBase {

    private static DelegateWebDriver webDriver;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Before
    public final void initTest() throws Exception {
        super.initTestBase();
        killDriverAndBrowser();
        Init.initBrowser();
        webDriver = Shared.getDelegateWebDriver();
    }

    /**
     * Постобработка.
     */
    @After
    public final void afterTest() throws InterruptedException {
        log.info("Закрытие браузера");
        webDriver.quit();
        killDriverAndBrowser();
        super.afterTestBase();
    }

    /**
     * Завершаем процесс WebDriver/Internet Explorer.
     */
    public final void killDriverAndBrowser() {
        log.info("Завершение процессов WebDriver/Internet Explorer");

        try {
            Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe").waitFor();
            Runtime.getRuntime().exec("taskkill /F /IM iexplore.exe").waitFor();
        } catch (Exception e) {
            log.debug("Не удалось завершить процессы WebDriver/InternetExplorer");
        }
    }

    public static DelegateWebDriver getDelegateWebDriver() {
        return webDriver;
    }
}
