package libs;

import static libs.WebDriverUtil.waitForPageToLoad;

import org.openqa.selenium.By;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;

/**
 * Created by OUT-Oreshin-AA on 12.04.2018.
 */
public class DelegateWebDriver implements WebDriver, JavascriptExecutor {
    private static final Logger LOG = LoggerFactory.getLogger(DelegateWebDriver.class);
    private By by;
    private WebDriver webDriver;
    private static final int NUMBER_OF_TRIES = 10;

    public DelegateWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    private WebDriver getWd() {
        return this.webDriver;
    }

    @Override
    public final void get(String url) {
        getWd().get(url);
    }

    @Override
    public final String getCurrentUrl() {
        return getWd().getCurrentUrl();
    }

    @Override
    public final String getTitle() {
        return getWd().getTitle();
    }

    @Override
    public final List<WebElement> findElements(By by) {
        this.by = by;
        for (int i = 0; i < NUMBER_OF_TRIES; i++) {
            waitForPageToLoad();
            try {
                return getWd().findElements(by);
            } catch (InvalidArgumentException | NullPointerException | JavascriptException e) {
                LOG.debug("Элемент протух");
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public final WebElement findElement(By by) {
        this.by = by;
        for (int i = 0; i < NUMBER_OF_TRIES; i++) {
            waitForPageToLoad();
            try {
                return getWd().findElement(by);
            } catch (InvalidArgumentException | NullPointerException | JavascriptException e) {
                LOG.debug("Элемент протух");
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public final String getPageSource() {
        return getWd().getPageSource();
    }

    @Override
    public final void close() {
        getWd().close();
    }

    @Override
    public final void quit() {
        getWd().quit();
    }

    @Override
    public final Set<String> getWindowHandles() {
        return getWd().getWindowHandles();
    }

    @Override
    public final String getWindowHandle() {
        return getWd().getWindowHandle();
    }

    @Override
    public final TargetLocator switchTo() {
        return getWd().switchTo();
    }

    @Override
    public final Navigation navigate() {
        return getWd().navigate();
    }

    @Override
    public final Options manage() {
        return getWd().manage();
    }

    public final WebElement refreshElement() {
        return getWd().findElement(this.by);
    }

    @Override
    public final Object executeScript(String script, Object... args) {
        return ((JavascriptExecutor) getWd()).executeScript(script, args);
    }

    @Override
    public final Object executeAsyncScript(String script, Object... args) {
        return ((JavascriptExecutor) getWd()).executeAsyncScript(script, args);
    }
}
