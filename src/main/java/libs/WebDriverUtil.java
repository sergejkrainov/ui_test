package libs;

import static org.junit.Assert.assertTrue;
import static testbase.TestBase.ec;

import config.Shared;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.pagefactory.DefaultElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.Table;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import org.hamcrest.Matchers;

public final class WebDriverUtil {
    private static final Logger LOG = LoggerFactory.getLogger(WebDriverUtil.class);
    private static DelegateWebDriver webDriver;
    private static WebDriverWait webDriverWait;
    private static Select select;

    private WebDriverUtil() {

    }

    /**
     *
     * @param url - url страницы.
     */
    public static void open(String url) {

        webDriver = Shared.getDelegateWebDriver();

        LOG.info("Открытие URL \"" + url + "\"");

        webDriver.get(url);

        LOG.info("Название страницы \"" + webDriver.getTitle() + "\"");
        LOG.info("Расширение экрана");

        webDriver.manage().window().maximize();

        /**
         * Проверяем, есть ли ошибка сертификата.
         */
        String pageTitle = webDriver.getTitle();
        if (pageTitle.contains("Ошибка сертификата") || pageTitle.contains("Certificate Error")) {
            webDriver.navigate().to("javascript:document.getElementById('overridelink').click()");
        }
    }

    /**
     *
     * @param webElement - вебэлемент.
     */
    public static void click(WebElement webElement) {

        webDriverWait = Shared.getWebDriverWait();

        LOG.info("Клик по элементу");
        boolean staleness = true;
        while (staleness) {
            try {
                visibilityOf(webElement);
                webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));
                webElement.click();
                staleness = false;
            } catch (InvalidArgumentException | StaleElementReferenceException | NoSuchElementException
                    | NullPointerException | JavascriptException e) {
                LOG.debug("Исключение при клике на элемент");
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param webElement - вебэлемент.
     */
    public static void clear(WebElement webElement) {


        webDriverWait = Shared.getWebDriverWait();

        LOG.info("Очистка элемента");
        boolean staleness = true;
        while (staleness) {
            try {
                visibilityOf(webElement);
                webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));
                webElement.clear();
                staleness = false;
            } catch (InvalidArgumentException | StaleElementReferenceException | NoSuchElementException
                    | NullPointerException | JavascriptException e) {
                LOG.debug("Исключение при очистке поля");
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param pageName - имя страницы.
     */
    public static void checkPageName(String pageName) {

        webDriverWait = Shared.getWebDriverWait();

        LOG.info("Проверка имени страницы");
        webDriverWait.until(ExpectedConditions.titleIs(pageName));
        assertTrue("Имя страницы не совпало \"" + webDriver.getTitle() + "\"", webDriver.getTitle().contains(pageName));
    }

    /**
     *
     * @param url - url страницы.
     */
    public static void checkPageUrl(String url) {

        webDriverWait = Shared.getWebDriverWait();

        LOG.info("Проверка URL страницы");
        webDriverWait.until(ExpectedConditions.urlToBe(url));
        assertTrue("URL страницы не совпало \"" + webDriver.getCurrentUrl() + "\"", webDriver.getCurrentUrl().contains(url));
    }

    /**
     *
     * @param text - проверяемый текст.
     */
    public static void checkPageContains(String text) {

        webDriver = Shared.getDelegateWebDriver();

        LOG.info("Проверка содержимого страницы");
        ec.checkThat("Страница не содержит текст \"" + webDriver.getPageSource()
                + "\"", webDriver.getPageSource().contains(text), Matchers.equalTo(true));
    }

    /**
     *
     * @param xpath - путь к элементу.
     */
    public static void checkElementPresent(String xpath) {

        webDriver = Shared.getDelegateWebDriver();

        LOG.info("Проверка наличия элемента");
        ec.checkThat("Страница не содержит элемент \"" + xpath + "\"", !webDriver.findElements(By.xpath(xpath)).isEmpty(), Matchers.equalTo(true));
    }

    /**
     *
     * @param webElement - вебэлемент.
     */
    public static void visibilityOf(WebElement webElement) {

        webDriverWait = Shared.getWebDriverWait();

        LOG.info("Проверка отображения");
        boolean staleness = true;
        while (staleness) {
            try {
                logBy(webElement);
                waitForPageToLoad();
                webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
                staleness = false;
            } catch (StaleElementReferenceException | JavascriptException e) {
                LOG.debug("Исключение при проверке отображения");
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param value - значение.
     * @param webElement - вебэлемент.
     */
    public static void select(String value, WebElement webElement) {
        LOG.info("Выбор \"" + value + "\"");
        boolean staleness = true;
        while (staleness) {
            try {
                visibilityOf(webElement);
                select = new Select(webElement);
                select.selectByValue(value);
                staleness = false;
            } catch (NullPointerException | StaleElementReferenceException e) {
                LOG.debug("Исключение при выборе");
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param webElement - вебэлемент.
     * @param value - значение.
     */
    public static void sendKeys(WebElement webElement, String value) {
        LOG.info("Ввод \"" + value + "\"");
        visibilityOf(webElement);
        webElement.clear();
        webElement.sendKeys(value);
    }

    public static void assertVisibility(WebElement webElement) {
        visibilityOf(webElement);
        assertTrue("Сообщение не отобразилось", webElement.isDisplayed());
    }

    /**
     *
     * @param expectedHeaders - ожидаемые заголовки.
     * @param rootElement - корневой элемент.
     */
    public static void checkHeaders(ArrayList<String> expectedHeaders, WebElement rootElement) {
        Table.setRootElement(rootElement);
        for (String header : expectedHeaders) {
            assertTrue("Заговолок \"" + header + "\" не отобразился", Table.getHeaderByName(header).isDisplayed());
        }
    }

    public static void logBy(WebElement webElement) {
        LOG.info(getByString(webElement));
    }

    /**
     *
     * @param webElement - вебэлемент.
     * @return - имя локатора.
     */
    public static String getByString(WebElement webElement) {
        String returnResult = "";
        try {
            if (java.lang.reflect.Proxy.isProxyClass(webElement.getClass())) {
                LocatingElementHandler h;
                h = (LocatingElementHandler) java.lang.reflect.Proxy.getInvocationHandler(webElement);

                Field locator = LocatingElementHandler.class.getDeclaredField("locator");
                locator.setAccessible(true);
                ElementLocator elLo = (ElementLocator) locator.get(h);
                Field byField = DefaultElementLocator.class.getDeclaredField("by");
                byField.setAccessible(true);
                By by = (By) byField.get(elLo);
                returnResult = by.toString();
            } else {
                RemoteWebElement remWeb = (RemoteWebElement) webElement;
                Field foundByField = RemoteWebElement.class.getDeclaredField("foundBy");
                foundByField.setAccessible(true);
                returnResult = (String) foundByField.get(remWeb);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return returnResult;
    }

    /**
     *
     * @param list - список вебэлементов.
     * @return - isContainsNullValues
     */
    public static boolean isContainsNullValues(List<WebElement> list) {
        for (WebElement webElement : list) {
            if (webElement == null) {
                LOG.debug("Массив элементов содержит null'ы");
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param webElement - вебэлемент.
     * @return - isWebElementNull.
     */
    public static boolean isWebElementNull(WebElement webElement) {
        if (webElement == null) {
            LOG.debug("Элемент null");
            return true;
        }
        return false;
    }

    /**
     * Ожидание загрузки страницы.
     */
    public static void waitForPageToLoad() {

        webDriverWait = Shared.getWebDriverWait();

        boolean staleness = true;
        while (staleness) {
            try {
                webDriverWait.until((ExpectedCondition<Boolean>) driver -> webDriver.executeScript("return document.readyState").equals("complete"));
                staleness = false;
            } catch (JavascriptException e) {
                e.printStackTrace();
            }
        }
    }
}
