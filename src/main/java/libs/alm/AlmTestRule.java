package libs.alm;

import static testbase.TestBase.ec;

import libs.TestErrorCollector;
import org.junit.Assert;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.reflect.Method;
import java.util.Objects;

public class AlmTestRule implements TestRule {
    private static final String CONFIG_FILE = "/alm.properties";
    private static final String FAILED = "Failed";
    private static final String PASSED = "Passed";
    private final AlmConfig baseConfig;

    private static final Logger LOG = LoggerFactory.getLogger(AlmTestRule.class);

    public AlmTestRule() {
        baseConfig = AlmConfig.fromFile(CONFIG_FILE);
    }

    @Override
    public final Statement apply(Statement base, Description description) {
        Objects.requireNonNull(base);
        Objects.requireNonNull(description);
        TestErrorCollector errCol = new TestErrorCollector();
        errCol.getErrors().clear();
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                String status;
                String messageError = "";
                try {
                    base.evaluate();
                    status = PASSED;
                } catch (Throwable ex) {
                    status = FAILED;
                    messageError = ex.getMessage();
                    ex.printStackTrace();
                }
                if (ec.hasErrors()) {
                    status = FAILED;
                    for (Throwable err : ec.getErrors()) {
                        messageError += (err.getMessage() + "\n");
                    }
                }
                if (isAlmTest(description)) {
                    String answer = "";
                    if (sendAlm(description, status)) {
                        answer = "Успешно!";
                    } else {
                        answer = "Ошибка!!!";
                    }
                    String fullTestName = description.getClassName() + "." + description.getMethodName();
                    LOG.info("Отправка HP ALM[{}]: {}", fullTestName, answer);
                }
                if (status.equals(FAILED)) {
                    Assert.fail(messageError);
                }

            }
        };

    }

    private boolean sendAlm(Description description, String status) throws Throwable {
        assert description != null;
        assert status != null;

        if (baseConfig != null) {
            AlmConfig config = new AlmConfig(baseConfig);
            config.load(description.getTestClass());
            config.load(description.getTestClass().getMethod(description.getMethodName()));
            System.out.println("ClassName = " + description.getTestClass());
            System.out.println("TestName = " + description.getTestClass().getMethod(description.getMethodName()));
            return new Api().addResultsToAlm(
                    config.get(AlmConfigField.SERVER),
                    config.get(AlmConfigField.DOMAIN),
                    config.get(AlmConfigField.PROJECT),
                    config.get(AlmConfigField.USER),
                    config.get(AlmConfigField.PASSWORD),
                    Integer.parseInt(config.get(AlmConfigField.TEST_GROUP_ID)),
                    Integer.parseInt(config.get(AlmConfigField.TEST_ID)),
                    status);
        } else {
            LOG.error("Ошибка загрузки конфигурации из файла [{}].", CONFIG_FILE);
            return false;
        }
    }

    private boolean isAlmTest(Description description) {
        assert description != null;

        try {
            Method m = description.getTestClass().getMethod(description.getMethodName());
            return m.isAnnotationPresent(AlmTest.class);
        } catch (NoSuchMethodException e) {
            return false;
        }
    }

}
