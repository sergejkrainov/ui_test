package libs.alm;

public final class ALMCheckCompleteRuns {

    private ALMCheckCompleteRuns() {

    }

    /**
     *
     * @param args - параметры консоли.
     */
    public static void main(String[] args) {
        final int zero = 0;
        final int one = 1;
        final int two = 2;
        final int three = 3;
        final int four = 4;
        final int five = 5;
        final int six = 6;
        final int seven = 7;

        try {
            if (args.length < seven) {
                System.out.println("Недостаточно входных параметров!");
                System.exit(one);
            }

            String serverUrl = args[zero];
            String domain = args[one];
            String project = args[two];
            String login = args[three];
            String password = args[four];
            int cycleId = Integer.parseInt(args[five]);
            String buildId = args[six];

            String res = new Api().checkCompleteTestRuns(serverUrl, domain, project, login, password, cycleId, buildId);
            System.out.println(res);
            System.exit(0);

        } catch (Exception ex) {
            System.out.println(ex);
            System.exit(1);
        }

    }
}
