package libs.alm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class AlmRequest {

    private String serverUrl;
    private String domain;
    private String project;
    private String login;
    private String password;
    private int cycleId = 0;
    private int testId = 0;
    private int testConfigId = 0;
    private String status;
    private String buildId;
    private String attachmentPath;
    private String attachmentName;
    private LinkedList<AlmStep> almSteps;
    private String runComment;
    private ArrayList<String> attachments;

    @SuppressWarnings("unchecked")
    public AlmRequest(HashMap<String, Object> params) {
        if (params.containsKey(AlmParams.SERVER_URL.getName())) {
            serverUrl = (String) params.get(AlmParams.SERVER_URL.getName());
        }
        if (params.containsKey(AlmParams.DOMAIN.getName())) {
            domain = (String) params.get(AlmParams.DOMAIN.getName());
        }
        if (params.containsKey(AlmParams.PROJECT.getName())) {
            project = (String) params.get(AlmParams.PROJECT.getName());
        }
        if (params.containsKey(AlmParams.LOGIN.getName())) {
            login = (String) params.get(AlmParams.LOGIN.getName());
        }
        if (params.containsKey(AlmParams.PASSWORD.getName())) {
            password = (String) params.get(AlmParams.PASSWORD.getName());
        }
        if (params.containsKey(AlmParams.CYCLE_ID.getName())) {
            cycleId = Integer.parseInt(params.get(AlmParams.CYCLE_ID.getName()).toString());
        }
        if (params.containsKey(AlmParams.TEST_ID.getName())) {
            testId = Integer.parseInt(params.get(AlmParams.TEST_ID.getName()).toString());
        }
        if (params.containsKey(AlmParams.TEST_CONFIG_ID.getName())) {
            testConfigId = Integer.parseInt(params.get(AlmParams.TEST_CONFIG_ID.getName()).toString());
        }
        if (params.containsKey(AlmParams.STATUS.getName())) {
            status = (String) params.get(AlmParams.STATUS.getName());
        }
        if (params.containsKey(AlmParams.BUILD_ID.getName())) {
            buildId = (String) params.get(AlmParams.BUILD_ID.getName());
        }
        if (params.containsKey(AlmParams.ATTACHMENT_PATH.getName())) {
            attachmentPath = (String) params.get(AlmParams.ATTACHMENT_PATH.getName());
        }
        if (params.containsKey(AlmParams.ATTACHMENT_NAME.getName())) {
            attachmentName = (String) params.get(AlmParams.ATTACHMENT_NAME.getName());
        }
        if (params.containsKey(AlmParams.ALM_STEPS.getName())) {
            almSteps = (LinkedList<AlmStep>) params.get(AlmParams.ALM_STEPS.getName());
        }
        if (params.containsKey(AlmParams.RUN_COMMENT.getName())) {
            runComment = (String) params.get(AlmParams.RUN_COMMENT.getName());
        }
        if (params.containsKey(AlmParams.ATTACHMENTS.getName())) {
            attachments = (ArrayList<String>) params.get(AlmParams.ATTACHMENTS.getName());
        }
    }

    public AlmRequest() {
    }

    public AlmRequest(String serverUrl, String domain, String project, String login, String password, Integer cycleId,
                      Integer testId, String status) {
        this.serverUrl = serverUrl;
        this.domain = domain;
        this.project = project;
        this.login = login;
        this.password = password;
        this.cycleId = cycleId;
        this.testId = testId;
        this.status = status;
    }

    public AlmRequest(String serverUrl, String domain, String project, String login, String password, Integer cycleId,
                      Integer testId, String status, String runComment) {
        this.serverUrl = serverUrl;
        this.domain = domain;
        this.project = project;
        this.login = login;
        this.password = password;
        this.cycleId = cycleId;
        this.testId = testId;
        this.status = status;
        this.runComment = runComment;
    }

    public AlmRequest(String serverUrl, String domain, String project, String login, String password, Integer cycleId,
                      Integer testId, LinkedList<AlmStep> almSteps) {
        this.serverUrl = serverUrl;
        this.domain = domain;
        this.project = project;
        this.login = login;
        this.password = password;
        this.cycleId = cycleId;
        this.testId = testId;
        this.almSteps = almSteps;
    }

    public AlmRequest(String serverUrl, String domain, String project, String login, String password, Integer cycleId,
                      Integer testId, LinkedList<AlmStep> almSteps, String runComment) {
        this.serverUrl = serverUrl;
        this.domain = domain;
        this.project = project;
        this.login = login;
        this.password = password;
        this.cycleId = cycleId;
        this.testId = testId;
        this.almSteps = almSteps;
        this.runComment = runComment;
    }

    public AlmRequest(String serverUrl, String domain, String project, String login, String password, Integer cycleId,
                      Integer testId, Integer testConfigId, String buildId, String status) {
        this.serverUrl = serverUrl;
        this.domain = domain;
        this.project = project;
        this.login = login;
        this.password = password;
        this.cycleId = cycleId;
        this.testId = testId;
        this.testConfigId = testConfigId;
        this.buildId = buildId;
        this.status = status;
    }

    public AlmRequest(String serverUrl, String domain, String project, String login, String password, Integer cycleId,
                      Integer testId, String buildId, String status, String runComment) {
        this.serverUrl = serverUrl;
        this.domain = domain;
        this.project = project;
        this.login = login;
        this.password = password;
        this.cycleId = cycleId;
        this.testId = testId;
        this.buildId = buildId;
        this.status = status;
        this.runComment = runComment;
    }

    public AlmRequest(String serverUrl, String domain, String project, String login, String password, Integer cycleId,
                      Integer testId, Integer testConfigId, String buildId, String status, String runComment) {
        this.serverUrl = serverUrl;
        this.domain = domain;
        this.project = project;
        this.login = login;
        this.password = password;
        this.cycleId = cycleId;
        this.testId = testId;
        this.testConfigId = testConfigId;
        this.buildId = buildId;
        this.status = status;
        this.runComment = runComment;
    }

    public AlmRequest(String serverUrl, String domain, String project, String login, String password, Integer cycleId,
                      Integer testId, Integer testConfigId, String buildId, LinkedList<AlmStep> almSteps) {
        this.serverUrl = serverUrl;
        this.domain = domain;
        this.project = project;
        this.login = login;
        this.password = password;
        this.cycleId = cycleId;
        this.testId = testId;
        this.testConfigId = testConfigId;
        this.buildId = buildId;
        this.almSteps = almSteps;
    }

    public AlmRequest(String serverUrl, String domain, String project, String login, String password, Integer cycleId,
                      Integer testId, Integer testConfigId, String buildId, LinkedList<AlmStep> almSteps, String runComment) {
        this.serverUrl = serverUrl;
        this.domain = domain;
        this.project = project;
        this.login = login;
        this.password = password;
        this.cycleId = cycleId;
        this.testId = testId;
        this.testConfigId = testConfigId;
        this.buildId = buildId;
        this.almSteps = almSteps;
        this.runComment = runComment;
    }

    public AlmRequest(String serverUrl, String domain, String project, String login, String password, Integer cycleId,
                      Integer testId, String buildId, LinkedList<AlmStep> almSteps) {
        this.serverUrl = serverUrl;
        this.domain = domain;
        this.project = project;
        this.login = login;
        this.password = password;
        this.cycleId = cycleId;
        this.testId = testId;
        this.buildId = buildId;
        this.almSteps = almSteps;
    }

    public AlmRequest(String serverUrl, String domain, String project, String login, String password, Integer cycleId, Integer testId,
                      String buildId, LinkedList<AlmStep> almSteps, String runComment) {
        this.serverUrl = serverUrl;
        this.domain = domain;
        this.project = project;
        this.login = login;
        this.password = password;
        this.cycleId = cycleId;
        this.testId = testId;
        this.buildId = buildId;
        this.almSteps = almSteps;
        this.runComment = runComment;
    }

    public final AlmRequest withServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
        return this;
    }

    public final AlmRequest withDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public final AlmRequest withProject(String project) {
        this.project = project;
        return this;
    }

    public final AlmRequest withLogin(String login) {
        this.login = login;
        return this;
    }

    public final AlmRequest withPassword(String password) {
        this.password = password;
        return this;
    }

    public final AlmRequest withCycleId(Integer cycleId) {
        this.cycleId = cycleId;
        return this;
    }

    public final AlmRequest withTestId(Integer testId) {
        this.testId = testId;
        return this;
    }

    public final AlmRequest withTestConfigId(Integer testConfigId) {
        this.testConfigId = testConfigId;
        return this;
    }

    public final AlmRequest withTestConfigId(int testConfigId) {
        this.testConfigId = testConfigId;
        return this;
    }

    public final AlmRequest withStatus(String status) {
        this.status = status;
        return this;
    }

    public final AlmRequest withBuildId(String buildId) {
        this.buildId = buildId;
        return this;
    }

    public final AlmRequest withAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
        return this;
    }

    public final AlmRequest withAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
        return this;
    }

    public final AlmRequest withAlmSteps(LinkedList<AlmStep> almSteps) {
        this.almSteps = almSteps;
        return this;
    }

    public final AlmRequest withRunComment(String runComment) {
        this.runComment = runComment;
        return this;
    }

    public final AlmRequest withAttachments(ArrayList<String> attachments) {
        this.attachments = attachments;
        return this;
    }

    /**
     * кладем объект в Map'у, если кому-то это нужно.
     *
     * @return - объект в HashMap.
     */
    public final HashMap<String, Object> getHashMap() {
        HashMap<String, Object> result = new LinkedHashMap<>();
        if (serverUrl != null) {
            result.put(AlmParams.SERVER_URL.getName(), serverUrl);
        }
        if (domain != null) {
            result.put(AlmParams.DOMAIN.getName(), domain);
        }
        if (project != null) {
            result.put(AlmParams.PROJECT.getName(), project);
        }
        if (login != null) {
            result.put(AlmParams.LOGIN.getName(), login);
        }
        if (password != null) {
            result.put(AlmParams.PASSWORD.getName(), password);
        }
        if (status != null) {
            result.put(AlmParams.STATUS.getName(), status);
        }
        if (cycleId != 0) {
            result.put(AlmParams.CYCLE_ID.getName(), String.valueOf(cycleId));
        }
        if (testId != 0) {
            result.put(AlmParams.TEST_ID.getName(), String.valueOf(testId));
        }

        if (testConfigId != 0) {
            result.put(AlmParams.TEST_CONFIG_ID.getName(), String.valueOf(testConfigId));
        }

        if (buildId != null) {
            result.put(AlmParams.BUILD_ID.getName(), buildId);
        }
        if (attachmentPath != null) {
            result.put(AlmParams.ATTACHMENT_PATH.getName(), attachmentPath);
        }
        if (attachmentName != null) {
            result.put(AlmParams.ATTACHMENT_NAME.getName(), attachmentName);
        }
        if (runComment != null) {
            result.put(AlmParams.RUN_COMMENT.getName(), runComment);
        }
        if (almSteps != null && almSteps.size() > 0) {
            result.put(AlmParams.ALM_STEPS.getName(), almSteps);
        }
        if (attachments != null && attachments.size() > 0) {
            result.put(AlmParams.ATTACHMENTS.getName(), attachments);
        }
        return result;
    }

    public final String getServerUrl() {
        return serverUrl;
    }

    public final String getDomain() {
        return domain;
    }

    public final String getProject() {
        return project;
    }

    public final String getLogin() {
        return login;
    }

    public final String getPassword() {
        return password;
    }

    public final int getCycleId() {
        return cycleId;
    }

    public final int getTestId() {
        return testId;
    }

    public final int getTestConfigId() {
        return testConfigId;
    }

    public final String getStatus() {
        return status;
    }

    public final String getBuildId() {
        return buildId;
    }

    public final String getAttachmentPath() {
        return attachmentPath;
    }

    public final String getAttachmentName() {
        return attachmentName;
    }

    public final LinkedList<AlmStep> getAlmSteps() {
        return almSteps;
    }

    public final String getRunComment() {
        return runComment;
    }

    public final ArrayList<String> getAttachments() {
        return attachments;
    }
}
