package libs.alm;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.imageio.ImageIO;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.awt.Robot;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.math.BigInteger;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by sbt-orlov-vi on 27.12.2016.
 */
public class Api {
    private Session session;
    private CookieManager manager;

    public static final int MAX_ACTUAL_DESCRIPTION_LENGTH = 200;

    private static final int STATUS_ATTACHMENT_OK = 201;
    private static final int STATUS_OK = 200;

    private String errorLogout = "";
    private String respTextLogout = "";
    private String respCodeLogout = "";

    private final Logger log = LoggerFactory.getLogger(Api.class);

    public final void setErrorText(String errorLogout) {
        this.errorLogout = errorLogout;
    }

    public final String getErrorText() {
        return this.errorLogout;
    }

    public final void setRespTextLogout(String respTextLogout) {
        this.respTextLogout = respTextLogout;
    }

    public final String getRespTextLogout() {
        return this.respTextLogout;
    }

    public final void setRespCodeLogout(String respCodeLogout) {
        this.respCodeLogout = respCodeLogout;
    }

    public final String getRespCodeLogout() {
        return this.respCodeLogout;
    }

    /**
     * Get testIds, which included in testSet.
     *
     * @param serverUrl - ALM server.
     * @param domain    - domain.
     * @param project   - project.
     * @param login     - user login.
     * @param password  - user password.
     * @param cycleId   - ALM testset ID.
     * @param rootPath  - rootPath.
     * @return String - Test IDs.
     */
    public final String getComparedTestId(String serverUrl, String domain, String project, String login,
                                          String password, Integer cycleId, String rootPath) {
        String result = "";
        try {
            init(serverUrl, domain, project, login, password);
            Entities testInstance = getTestInstance(cycleId);
            ArrayList<String> testIdsAlm = getTestIds(testInstance);
            ArrayList<String> res = new Helper().getAlmTagsFromRootPath(rootPath);
            for (String str : testIdsAlm) {
                if (res.contains(str)) {
                    if (!result.isEmpty()) {
                        result += ",@" + str;
                    } else {
                        result += "@" + str;
                    }
                }
            }
        } catch (Exception ex) {

            log.error("Ошибка:" + ex.getMessage());
        } finally {
            logout();
        }
        return result;
    }

    /**
     * Get Hashmap with complete, not complete or failed tests with BUILD ID.
     *
     * @param serverUrl - ALM server.
     * @param domain    - domain.
     * @param project   - project.
     * @param login     - user login.
     * @param password  - user password.
     * @param cycleId   - ALM testset ID.
     * @param buildId   - ID build.
     * @return HashMap - HashMap.
     */
    public final HashMap getCompleteRunMap(String serverUrl, String domain, String project, String login, String password,
                                           Integer cycleId, String buildId) {
        ArrayList<String> testList;
        ArrayList<String> testListPassed = new ArrayList<>();
        ArrayList<String> testListFailed = new ArrayList<>();
        HashMap<String, Object> lists = new HashMap<>();

        try {
            // Initialization
            init(serverUrl, domain, project, login, password);

            // Get cycle test instance
            Entities cycleInstance = getTestInstance(cycleId);

            // Get test IDs
            testList = getTestIds(cycleInstance);

            testList.forEach(index -> {
                Entities runs = new Entities();
                try {
                    Entities cycleTestInstance = getTestInstance(cycleId, Integer.parseInt(index));
                    // Get test instance ID
                    String testInstanceId = getTestInstanceId(cycleTestInstance);
                    // Get Runs
                    runs = getRuns(Integer.parseInt(testInstanceId));
                } catch (Exception ex) {
                    log.error(ex.getMessage());
                }

                for (int y = runs.getEntity().size() - 1; y > -1; y--) {
                    Entity runData = runs.getEntity().get(y);
                    if (buildId.equals(runData.getStringFieldValue("user-template-01"))
                            && "Passed".equals(runData.getStringFieldValue("status"))) {
                        testListPassed.add(index);
                        break;
                    }
                    if (buildId.equals(runData.getStringFieldValue("user-template-01"))
                            && "Failed".equals(runData.getStringFieldValue("status"))) {
                        testListFailed.add(index);
                        break;
                    }
                }
            });

            if (!testListPassed.isEmpty()) {
                testListPassed.forEach(testList::remove);
            }
            if (!testListFailed.isEmpty()) {
                testListFailed.forEach(testList::remove);
            }

            if (testList.isEmpty() && !testListPassed.isEmpty() && testListFailed.isEmpty()) {
                log.info("All test runs with Build " + buildId + " in Test Set " + cycleId + " was completed!");
            } else {
                if (!testList.isEmpty()) {
                    log.info("Not completed tests or tests with no runs for Build " + buildId + ": " + testList);
                }
                if (!testListPassed.isEmpty()) {
                    log.info("Tests with runs for Build " + buildId + " in status \"PASSED\": " + testListPassed);
                }
                if (!testListFailed.isEmpty()) {
                    log.info("Tests with runs for Build " + buildId + " in status \"FAILED\": " + testListFailed);
                }
            }

            lists.put("all", testList);
            lists.put("passed", testListPassed);
            lists.put("failed", testListFailed);

        } catch (Exception ex) {
            log.error(ex.getMessage());
        } finally {
            logout();
        }
        return lists;
    }

    /**
     * Проверка прохождения тестового набора по ID сборки.
     *
     * @param serverUrl - ALM server.
     * @param domain    - domain.
     * @param project   - project.
     * @param login     - user login.
     * @param password  - user password.
     * @param cycleId   - ALM testset ID.
     * @param buildId   - ID build.
     * @return String - статус.
     */
    public final String checkCompleteTestRuns(String serverUrl, String domain, String project, String login, String password,
                                              Integer cycleId, String buildId) {
        String status;
        HashMap testMap = getCompleteRunMap(serverUrl, domain, project, login, password, cycleId, buildId);
        if (((List) testMap.get("all")).isEmpty()) {
            status = "OK";
        } else if (!((List) testMap.get("failed")).isEmpty()) {
            status = "HAVE_FAILED_TESTS";
        } else {
            status = "NOT_COMPLETE";
        }
        return status;
    }

    /**
     * Прикрепление аттача к run.
     *
     * @param cycleId        - Test Set ID.
     * @param testId         - Test ID.
     * @param testConfigId   - Configuration ID.
     * @param attachmentPath - путь к аттачу.
     * @param attachmentName - название аттача в HP ALM.
     * @throws Exception - тип ошибки.
     */
    private void prepareAndSendAttachment(int cycleId, int testId, int testConfigId, String attachmentPath, String
            attachmentName) throws Exception {
        String lastRunId;
        Entities runs;
        /**
         * Если явно прописано имя файла, то используем его.
         */
        if (attachmentName == null) {
            /**
             * По умолчанию берем имя файла из пути.
             */
            attachmentName = new File(attachmentPath).getName();
        }
        runs = this.getRuns(cycleId, testId, testConfigId);
        lastRunId = this.getLastRunId(runs);
        sendAttachmentToRun(lastRunId, getAttachmentString(attachmentPath), attachmentName);
    }

    /**
     * Общий метод отправки результатов в HP ALM.
     *
     * @param almRequest - Объект с входными параметрами.
     * @return - boolean result.
     */
    public final boolean addResultsToAlm(AlmRequest almRequest) {
        boolean result;
        if (almRequest != null) {
            String serverUrl = almRequest.getServerUrl();
            String domain = almRequest.getDomain();
            String project = almRequest.getProject();
            String login = almRequest.getLogin();
            String password = almRequest.getPassword();
            String status = almRequest.getStatus();
            int cycleId = almRequest.getCycleId();
            int testId = almRequest.getTestId();
            int testConfigId = almRequest.getTestConfigId();
            String buildId = almRequest.getBuildId();
            String attachmentPath = almRequest.getAttachmentPath();
            String attachmentName = almRequest.getAttachmentName();
            String runComment = almRequest.getRunComment();

            LinkedList<AlmStep> almSteps = almRequest.getAlmSteps();

            /**
             * Содержит в себе пути к прикрепляемым файлам.
             */
            ArrayList<String> attachments = almRequest.getAttachments();

            result = true;
            try {
                if (status == null) {
                    status = "Passed";
                    for (AlmStep step : almSteps) {
                        if (!step.getStatus().equals("Passed")) {
                            status = step.getStatus();
                            break;
                        }
                    }
                }

                // Initialization
                init(serverUrl, domain, project, login, password);
                Entities testInstance;
                /**
                 * Если заполнен testConfig - получаем инстанс с фильтрацией по нему
                 */
                if (testConfigId != 0) {
                    testInstance = getTestInstance(cycleId, testId, testConfigId);
                } else {
                    testInstance = getTestInstance(cycleId, testId);
                }
                // Get test instance ID
                String testInstanceId = getTestInstanceId(testInstance);
                if (testInstanceId == null) {
                    log.info("TestID " + testId + " is not contained in TestSetID " + cycleId);
                    return false;
                }
                // Get test instance status
                String testInstanceStatus = getTestInstanceStatus(testInstance);

                Map<String, String> almItem = new HashMap<>();
                if (testInstanceStatus.equals("Not Completed")) {
                    almItem.put("status", "N/A");
                } else {
                    almItem.put("status", "Not Completed");
                }
                if (!updateTestInstance(testInstanceId, almItem)) {
                    result = false;
                }

                // Add status information
                Map<String, String> almItem2 = new HashMap<>();
                almItem2.put("Status", status);
                if (!updateTestInstance(testInstanceId, almItem2)) {
                    result = false;
                }

                Entities runs = getRuns(Integer.parseInt(testInstanceId));
                String prevRunId = getPrevRunId(runs);
                if (!deleteRun(prevRunId)) {
                    result = false;
                }

                // Get last run ID
                String lastRunId = getLastRunId(runs);

                // Comment for test set run
                if (runComment != null) {
                    Map<String, String> almItem3 = new HashMap<>();
                    almItem3.put("comments", runComment);
                    if (!updateRun(lastRunId, almItem3)) {
                        log.info("Cannot set comment for TestRunId: " + lastRunId + "!");
                        result = false;
                    }
                }

                /**
                 * Добавление поля "Номер билда".
                 */
                if (buildId != null) {
                    HashMap buildInfo = new HashMap();
                    /**
                     * Поле "Номер билда" в xml - user-template-01.
                     */
                    buildInfo.put("user-template-01", buildId);
                    if (!this.updateRun(lastRunId, buildInfo)) {
                        log.info("Cannot set build number for TestRunId: " + lastRunId + "!");
                        result = false;
                    }
                }

                /**
                 * Добавление аттача.
                 */
                if (attachmentPath != null) {
                    prepareAndSendAttachment(cycleId, testId, testConfigId, attachmentPath, attachmentName);
                }

                /**
                 * Добавление нескольких аттачей.
                 */
                if (attachments != null) {
                    for (String attachPath : attachments) {
                        /**
                         * В HP ALM имена файлов будут соответствовать оригинальным.
                         */
                        prepareAndSendAttachment(cycleId, testId, testConfigId, attachPath, null);
                    }
                }

                // Add step information (include screenshots)
                if (almSteps != null) {
                    Entities runSteps = getRunSteps(lastRunId);
                    Integer index = 0;
                    for (Entity rs : runSteps.getEntity()) {
                        if (index >= almSteps.size()) {
                            break;
                        }
                        AlmStep almStep = almSteps.get(index);
                        Map<String, String> stepItem = new HashMap<>();

                        String resultString = almStep.getResult();
                        if (resultString.length() > MAX_ACTUAL_DESCRIPTION_LENGTH) {
                            attachmentLogToStep(rs.getId() + "", resultString, "step_log.txt");
                            stepItem.put("Actual", almStep.getResult().substring(0, MAX_ACTUAL_DESCRIPTION_LENGTH));
                        } else {
                            stepItem.put("Actual", resultString);
                        }

                        stepItem.put("Status", almStep.getStatus());
                        updateRunStep(rs.getId() + "", stepItem);

                        if (!almStep.getScreenShot().isEmpty()) {
                            addScreenShotToRunStep(rs.getId() + "", almStep.getScreenShot(), "screenshot.png");
                        }
                        index++;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                log.info(ex.getMessage());
                result = false;
            } finally {
                logout();
            }
        } else {
            log.error("Передан пустой объект!");
            result = false;
        }
        return result;
    }

    /**
     *
     * @param serverUrl - serverUrl.
     * @param domain - domain.
     * @param project - project.
     * @param login - login.
     * @param password - password.
     * @param cycleId - cycleId.
     * @param testId - testId.
     * @param almSteps - almSteps.
     * @return - boolean result.
     */
    public final boolean addResultsToAlm(String serverUrl, String domain, String project, String login, String
            password, Integer cycleId, Integer testId, LinkedList<AlmStep> almSteps) {
        AlmRequest params = new AlmRequest()
                .withServerUrl(serverUrl)
                .withDomain(domain)
                .withProject(project)
                .withLogin(login)
                .withPassword(password)
                .withCycleId(cycleId)
                .withTestId(testId)
                .withAlmSteps(almSteps);
        return addResultsToAlm(params);
    }

    /**
     *
     * @param serverUrl - serverUrl.
     * @param domain - domain.
     * @param project - project.
     * @param login - login.
     * @param password - password.
     * @param cycleId - cycleId.
     * @param testId - testId.
     * @param status - status.
     * @return - boolean result.
     */
    public final boolean addResultsToAlm(String serverUrl, String domain, String project, String login, String
            password, Integer cycleId, Integer testId, String status) {
        AlmRequest params = new AlmRequest()
                .withServerUrl(serverUrl)
                .withDomain(domain)
                .withProject(project)
                .withLogin(login)
                .withPassword(password)
                .withCycleId(cycleId)
                .withTestId(testId)
                .withStatus(status);
        return addResultsToAlm(params);
    }

    /**
     *
     * @param serverUrl - serverUrl.
     * @param domain - domain.
     * @param project - project.
     * @param login - login.
     * @param password - password.
     * @param cycleId - cycleId.
     * @param testId - testId.
     * @param status - status.
     * @param runComment - runComment.
     * @return - boolean result.
     */
    public final boolean addResultsToAlm(String serverUrl, String domain, String project, String login, String
            password, Integer cycleId, Integer testId, String status, String runComment) {
        AlmRequest params = new AlmRequest()
                .withServerUrl(serverUrl)
                .withDomain(domain)
                .withProject(project)
                .withLogin(login)
                .withPassword(password)
                .withCycleId(cycleId)
                .withTestId(testId)
                .withStatus(status)
                .withRunComment(runComment);
        return addResultsToAlm(params);
    }

    /**
     *
     * @param serverUrl - serverUrl.
     * @param domain - domain.
     * @param project - project.
     * @param login - login.
     * @param password - password.
     * @param cycleId - cycleId.
     * @param testId - testId.
     * @param almSteps - almSteps.
     * @param runComment - runComment.
     * @return - boolean result.
     */
    public final boolean addResultsToAlm(String serverUrl, String domain, String project, String login, String
            password, Integer cycleId, Integer testId, LinkedList<AlmStep> almSteps, String runComment) {
        AlmRequest params = new AlmRequest()
                .withServerUrl(serverUrl)
                .withDomain(domain)
                .withProject(project)
                .withLogin(login)
                .withPassword(password)
                .withCycleId(cycleId)
                .withTestId(testId)
                .withAlmSteps(almSteps)
                .withRunComment(runComment);
        return addResultsToAlm(params);
    }

    /**
     *
     * @param serverUrl - serverUrl.
     * @param domain - domain.
     * @param project - project.
     * @param login - login.
     * @param password - password.
     * @param cycleId - cycleId.
     * @param testId - testId.
     * @param testConfigId - testConfigId.
     * @param buildId - buildId.
     * @param status - status.
     * @param runComment - runComment.
     * @return - boolean result.
     */
    public final boolean addResultsToAlm(String serverUrl, String domain, String project, String login, String
            password, Integer cycleId, Integer testId, Integer testConfigId, String buildId, String status, String
                                                 runComment) {
        AlmRequest params = new AlmRequest()
                .withServerUrl(serverUrl)
                .withDomain(domain)
                .withProject(project)
                .withLogin(login)
                .withPassword(password)
                .withCycleId(cycleId)
                .withTestId(testId)
                .withTestConfigId(testConfigId)
                .withBuildId(buildId)
                .withStatus(status)
                .withRunComment(runComment);
        return addResultsToAlm(params);
    }

    /**
     *
     * @param serverUrl - serverUrl.
     * @param domain - domain.
     * @param project - project.
     * @param login - login.
     * @param password - password.
     * @param cycleId - cycleId.
     * @param testId - testId.
     * @param testConfigId - testConfigId.
     * @param buildId - buildId.
     * @param status - status.
     * @return - boolean result.
     */
    public final boolean addResultsToAlm(String serverUrl, String domain, String project, String login, String
            password, Integer cycleId, Integer testId, Integer testConfigId, String buildId, String status) {
        AlmRequest params = new AlmRequest()
                .withServerUrl(serverUrl)
                .withDomain(domain)
                .withProject(project)
                .withLogin(login)
                .withPassword(password)
                .withCycleId(cycleId)
                .withTestId(testId)
                .withTestConfigId(testConfigId)
                .withBuildId(buildId)
                .withStatus(status);
        return addResultsToAlm(params);
    }

    /**
     *
     * @param serverUrl - serverUrl.
     * @param domain - domain.
     * @param project - project.
     * @param login - login.
     * @param password - password.
     * @param cycleId - cycleId.
     * @param testId - testId.
     * @param testConfigId - testConfigId.
     * @param buildId - buildId.
     * @param almSteps - almSteps.
     * @param runComment - runComment.
     * @return - boolean result.
     */
    public final boolean addResultsToAlm(String serverUrl, String domain, String project, String login, String
            password, Integer cycleId, Integer testId, Integer testConfigId, String
                                                 buildId, LinkedList<AlmStep> almSteps, String runComment) {
        AlmRequest params = new AlmRequest()
                .withServerUrl(serverUrl)
                .withDomain(domain)
                .withProject(project)
                .withLogin(login)
                .withPassword(password)
                .withCycleId(cycleId)
                .withTestId(testId)
                .withTestConfigId(testConfigId)
                .withBuildId(buildId)
                .withAlmSteps(almSteps)
                .withRunComment(runComment);
        return addResultsToAlm(params);
    }

    /**
     *
     * @param serverUrl - serverUrl.
     * @param domain - domain.
     * @param project - project.
     * @param login - login.
     * @param password - password.
     * @param cycleId - cycleId.
     * @param testId - testId.
     * @param testConfigId - testConfigId.
     * @param buildId - buildId.
     * @param almSteps - almSteps.
     * @return - boolean result.
     */
    public final boolean addResultsToAlm(String serverUrl, String domain, String project, String login, String
            password, Integer cycleId, Integer testId, Integer testConfigId, String buildId, LinkedList<AlmStep> almSteps) {
        AlmRequest params = new AlmRequest()
                .withServerUrl(serverUrl)
                .withDomain(domain)
                .withProject(project)
                .withLogin(login)
                .withPassword(password)
                .withCycleId(cycleId)
                .withTestId(testId)
                .withTestConfigId(testConfigId)
                .withBuildId(buildId)
                .withAlmSteps(almSteps);
        return addResultsToAlm(params);
    }

    /**
     *
     * @param serverUrl - serverUrl.
     * @param domain - domain.
     * @param project - project.
     * @param login - login.
     * @param password - password.
     * @param cycleId - cycleId.
     * @param testId - testId.
     * @param buildId - buildId.
     * @param status - status.
     * @param runComment - runComment.
     * @return - boolean result.
     */
    public final boolean addResultsToAlm(String serverUrl, String domain, String project, String login, String
            password, Integer cycleId, Integer testId, String buildId, String status, String runComment) {
        AlmRequest params = new AlmRequest()
                .withServerUrl(serverUrl)
                .withDomain(domain)
                .withProject(project)
                .withLogin(login)
                .withPassword(password)
                .withCycleId(cycleId)
                .withTestId(testId)
                .withBuildId(buildId)
                .withStatus(status)
                .withRunComment(runComment);
        return addResultsToAlm(params);
    }

    /**
     *
     * @param serverUrl - serverUrl.
     * @param domain - domain.
     * @param project - project.
     * @param login - login.
     * @param password - password.
     * @param cycleId - cycleId.
     * @param testId - testId.
     * @param buildId - buildId.
     * @param almSteps - almSteps.
     * @param runComment - runComment.
     * @return - boolean result.
     */
    public final boolean addResultsToAlm(String serverUrl, String domain, String project, String login, String
            password, Integer cycleId, Integer testId, String buildId, LinkedList<AlmStep> almSteps, String runComment) {
        AlmRequest params = new AlmRequest()
                .withServerUrl(serverUrl)
                .withDomain(domain)
                .withProject(project)
                .withLogin(login)
                .withPassword(password)
                .withCycleId(cycleId)
                .withTestId(testId)
                .withBuildId(buildId)
                .withAlmSteps(almSteps)
                .withRunComment(runComment);
        return addResultsToAlm(params);
    }

    /**
     *
     * @param serverUrl - serverUrl.
     * @param domain - domain.
     * @param project - project.
     * @param login - login.
     * @param password - password.
     * @param cycleId - cycleId.
     * @param testId - testId.
     * @param buildId - buildId.
     * @param almSteps - almSteps.
     * @return - boolean result.
     */
    public final boolean addResultsToAlm(String serverUrl, String domain, String project, String login, String
            password, Integer cycleId, Integer testId, String buildId, LinkedList<AlmStep> almSteps) {
        AlmRequest params = new AlmRequest()
                .withServerUrl(serverUrl)
                .withDomain(domain)
                .withProject(project)
                .withLogin(login)
                .withPassword(password)
                .withCycleId(cycleId)
                .withTestId(testId)
                .withBuildId(buildId)
                .withAlmSteps(almSteps);
        return addResultsToAlm(params);
    }

    /**
     *
     * @param inputMap - inputMap.
     * @return - result.
     */
    public final boolean addResultsToAlm(HashMap<String, Object> inputMap) {
        AlmRequest params = new AlmRequest(inputMap);
        return addResultsToAlm(params);
    }

    /**
     *
     * @return - скриншот.
     */
    public final String getScreenShotString() {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize())), "png", baos);
            return DatatypeConverter.printHexBinary(baos.toByteArray());
        } catch (Exception ex) {
            log.error("Ошибка:" + ex.getMessage());
        }
        return "";
    }

    /**
     * Преобразуем прикрепляемый файл в строку.
     *
     * @param filePath - путь к файлу.
     * @return - содержимое файла.
     */
    public final String getAttachmentString(String filePath) {
        final int byteSize = 16384;
        try (InputStream inputStream = new FileInputStream(filePath);
             ByteArrayOutputStream buffer = new ByteArrayOutputStream()) {
            byte[] data = new byte[byteSize];
            while (inputStream.available() > 0) {
                inputStream.read(data);
                buffer.write(data);
            }
            buffer.flush();
            return DatatypeConverter.printHexBinary(buffer.toByteArray());
        } catch (Exception e) {
            log.info("Проблема с чтением файла");
        }
        return "";
    }

    private String encryptData(String data, String modulus, String exponent) throws Exception {
        final int radixVal = 16;
        try {
            RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(modulus, radixVal), new BigInteger(exponent, radixVal));
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PublicKey pub = factory.generatePublic(spec);
            Cipher rsa = Cipher.getInstance("RSA");
            rsa.init(Cipher.ENCRYPT_MODE, pub);
            byte[] cipherText = rsa.doFinal(data.getBytes());
            return Hex.encodeHexString(cipherText);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return "";
        }
    }

    private <T> T marshal(Class<T> c, String xml) throws JAXBException {
        T res;
        if (c == xml.getClass()) {
            res = (T) xml;
        } else {
            JAXBContext ctx = JAXBContext.newInstance(c);
            Unmarshaller unm = ctx.createUnmarshaller();
            res = (T) unm.unmarshal(new StringReader(xml));
        }
        return res;
    }

    private String getFieldValue(Entities entities, String fieldName) {
        for (Entity entity : entities.getEntity()) {
            for (Field field : entity.getFields().getFieldList()) {
                if (field.getName().equals(fieldName)) {
                    return field.getValue().get(0);
                }
            }
        }
        return null;
    }

    private ArrayList<String> getFieldValues(Entities entities, String fieldName) {
        ArrayList<String> result = new ArrayList<>();
        for (Entity entity : entities.getEntity()) {
            for (Field field : entity.getFields().getFieldList()) {
                if (field.getName().equals(fieldName)) {
                    result.add(field.getValue().get(0));
                }
            }
        }
        return result;
    }

    private Entities mergeEntities(List<Entities> entitiesList) {
        Entities ent = new Entities();
        Integer count = 0;
        for (Entities e : entitiesList) {
            for (Entity entity : e.getEntity()) {
                ent.getEntity().add(entity);
                count++;
            }
        }
        ent.setTotalResults(count);
        return ent;
    }

    private String getPrevRunId(Entities entities) {
        return entities.getEntity().get(entities.getEntity().size() - 2).getId() + "";
    }

    private String getLastRunId(Entities entities) {
        return entities.getEntity().get(entities.getEntity().size() - 1).getId() + "";
    }

    private String getTestInstanceId(Entities entities) {
        return getFieldValue(entities, "id");
    }

    private ArrayList<String> getTestIds(Entities entities) {
        return getFieldValues(entities, "test-id");
    }

    private String getTestInstanceStatus(Entities entities) {
        return getFieldValue(entities, "status");
    }

    private Entities getTestInstance(int cycleId, int testId, int testConfigId) throws Exception {
        String str = URLEncoder.encode("{cycle-id[" + cycleId + "];test-id[" + testId + "];test-config-id[" + testConfigId + "]}", "UTF-8");
        WebResource wr = this.session.client.resource(UriBuilder.fromUri(this.session.almUrl + "/rest/domains/"
                + this.session.almDomain + "/projects/" + this.session.almProject + "/test-instances?query=" + str).build(new Object[0]));
        String xml = wr.get(String.class);
        return this.marshal(Entities.class, xml);
    }

    private Entities getTestInstance(int cycleId, int testId) throws Exception {
        String str = URLEncoder.encode("{cycle-id[" + cycleId + "];test-id[" + testId + "]}", "UTF-8");
        WebResource wr = session.client.resource(UriBuilder.fromUri(session.almUrl + "/rest/domains/"
                + session.almDomain + "/projects/" + session.almProject + "/test-instances?query=" + str).build());
        String xml = wr.get(String.class);
        return marshal(Entities.class, xml);
    }

    private Entities getTestInstance(int cycleId) throws Exception {
        String str = URLEncoder.encode("{cycle-id[" + cycleId + "]}", "UTF-8");
        WebResource wr = session.client.resource(UriBuilder.fromUri(session.almUrl + "/rest/domains/"
                + session.almDomain + "/projects/" + session.almProject + "/test-instances?page-size=4000&query=" + str).build());
        String xml = wr.get(String.class);
        return marshal(Entities.class, xml);
    }

    /**
     * Возвращает прогоны по Test Set Id, Test Id, Test Config Id.
     *
     * @param cycleId      - тест сет.
     * @param testId       - тест.
     * @param testConfigId - конфигурация.
     * @return Entities - Entities.
     * @throws Exception - тип ошибки.
     */
    private Entities getRuns(int cycleId, int testId, int testConfigId) throws Exception {
        String testConfStr = "";
        if (testConfigId != 0) {
            testConfStr = ";test-config-id[" + testConfigId + "]";
        }
        String testCycleId = "";
        if (testId != 0) {
            testCycleId = "cycle-id[" + cycleId + "];test-id[" + testId + "]";
        } else {
            testCycleId = "testcycl-id[" + cycleId + "]";
        }

        List entities = new LinkedList<>();
        Integer index = 1;
        final Integer pageSize = 50;
        Integer total = 2;
        String str = URLEncoder.encode("{" + testCycleId + ";" + testConfStr + "}", "UTF-8");
        while (index < total) {
            WebResource wr = session.client.resource(UriBuilder.fromUri(session.almUrl + "/rest/domains/" + session.almDomain
                    + "/projects/" + session.almProject + "/runs?page-size=" + pageSize + "&start-index=" + index + "&query=" + str).build());
            String xml = wr.get(String.class);
            Entities ent = marshal(Entities.class, xml);
            total = ent.getTotalResults();
            index += pageSize;
            entities.add(ent);
        }
        return this.mergeEntities(entities);
    }

    private Entities getRuns(int cycleId, int testId) throws Exception {
        return getRuns(cycleId, testId, 0);
    }

    private Entities getRuns(int testCycleId) throws Exception {
        return getRuns(testCycleId, 0, 0);
    }

    private Entities getRunSteps(String runId) throws Exception {
        List<Entities> entities = new LinkedList<>();
        Integer index = 1;
        final Integer pageSize = 50;
        Integer total = 2;
        String str = URLEncoder.encode("{parent-id[" + runId + "]}", "UTF-8");
        while (index < total) {
            WebResource wr = session.client.resource(UriBuilder.fromUri(session.almUrl + "/rest/domains/"
                    + session.almDomain + "/projects/" + session.almProject + "/run-steps?page-size=" + pageSize + "&start-index="
                    + index + "&query=" + str).build());
            String xml = wr.get(String.class);
            Entities ent = marshal(Entities.class, xml);
            total = ent.getTotalResults();
            index += pageSize;
            entities.add(ent);
        }
        return mergeEntities(entities);
    }

    private String convertToFieldXml(String eType, Map<String, String> dictionary) {
        StringBuilder builder = new StringBuilder();

        builder.append("<Entity Type=\"" + eType + "\">");
        builder.append("<Fields>");

        for (String key : dictionary.keySet()) {
            String value = dictionary.get(key);

            builder.append("<Field Name=\"" + key + "\">");
            builder.append("<Value>");
            builder.append(value);
            builder.append("</Value>");
            builder.append("</Field>");
        }

        builder.append("</Fields>");
        builder.append("</Entity>");

        return builder.toString();
    }

    private boolean deleteRun(String runId) throws Exception {
        WebResource wr = session.client.resource(UriBuilder.fromUri(session.almUrl + "/rest/domains/"
                + session.almDomain + "/projects/" + session.almProject + "/runs/" + runId).build());
        ClientResponse response = wr.accept("application/xml").type("application/xml").delete(ClientResponse.class);
        int status = response.getStatus();
        if (status == STATUS_OK) {
            return true;
        }
        return false;
    }

    private boolean updateTestInstance(String testInstanceId, Map<String, String> item) throws Exception {
        WebResource wr = session.client.resource(UriBuilder.fromUri(session.almUrl + "/rest/domains/"
                + session.almDomain + "/projects/" + session.almProject + "/test-instances/" + testInstanceId).build());
        ClientResponse response = wr.accept("application/xml").type("application/xml")
                .put(ClientResponse.class, convertToFieldXml("test-instance", item));
        int status = response.getStatus();
        if (status == STATUS_OK) {
            return true;
        }
        return false;
    }

    private boolean updateRun(String runStepId, Map<String, String> item) throws Exception {
        WebResource wr = session.client.resource(UriBuilder.fromUri(session.almUrl + "/rest/domains/"
                + session.almDomain + "/projects/" + session.almProject + "/runs/" + runStepId).build());
        ClientResponse response = wr.accept("application/xml").type("application/xml")
                .put(ClientResponse.class, convertToFieldXml("run", item));
        int status = response.getStatus();
        if (status == STATUS_OK) {
            return true;
        }
        return false;
    }

    private boolean updateRunStep(String runStepId, Map<String, String> item) throws Exception {
        WebResource wr = session.client.resource(UriBuilder.fromUri(session.almUrl + "/rest/domains/"
                + session.almDomain + "/projects/" + session.almProject + "/run-steps/" + runStepId).build());
        ClientResponse response = wr.accept("application/xml").type("application/xml")
                .put(ClientResponse.class, convertToFieldXml("run-step", item));
        int status = response.getStatus();
        if (status == STATUS_OK) {
            return true;
        }
        return false;
    }

    private boolean addScreenShotToRunStep(String runStepId, String screenshot, String fileName) throws Exception {
        WebResource wr = session.client.resource(UriBuilder.fromUri(session.almUrl + "/rest/domains/"
                + session.almDomain + "/projects/" + session.almProject + "/run-steps/" + runStepId + "/attachments").build());
        ClientResponse response = wr.header("Slug", fileName).type("application/octet-stream")
                .post(ClientResponse.class, DatatypeConverter.parseHexBinary(screenshot));
        int status = response.getStatus();
        if (status == STATUS_ATTACHMENT_OK) {
            return true;
        }
        return false;
    }

    /**
     * Добавление файла к прогону.
     *
     * @param runId      - Id прогона.
     * @param attachment - путь к прикрепляемому файлу.
     * @param fileName   - имя файла в HP ALM.
     * @return - boolean result.
     * @throws Exception - тип ошибки.
     */
    private boolean sendAttachmentToRun(String runId, String attachment, String fileName) throws Exception {
        WebResource wr = this.session.client.resource(UriBuilder.fromUri(this.session.almUrl + "/rest/domains/"
                + this.session.almDomain + "/projects/" + this.session.almProject + "/runs/" + runId + "/attachments").build());
        ClientResponse response = wr.header("Slug", fileName).type("application/octet-stream")
                .post(ClientResponse.class, DatatypeConverter.parseHexBinary(attachment));
        int status = response.getStatus();
        return status == STATUS_ATTACHMENT_OK;
    }

    /**
     * Добавление файла лога к шагу.
     *
     * @param runStepId  - Id шага.
     * @param attachment - текст лога.
     * @param fileName   - имя файла в HP ALM.
     * @return - boolean result.
     * @throws Exception - тип ошибки.
     */
    private boolean attachmentLogToStep(String runStepId, String attachment, String fileName) throws Exception {
        WebResource wr = session.client.resource(UriBuilder.fromUri(session.almUrl + "/rest/domains/" + session.almDomain + "/projects/"
                + session.almProject + "/run-steps/" + runStepId + "/attachments").build());
        ClientResponse response = wr.header("Slug", fileName).type("application/octet-stream").post(ClientResponse.class, attachment);
        return response.getStatus() == STATUS_ATTACHMENT_OK;
    }

    private void init(String url, String domain, String project, String login, String password) throws Exception {
        session = new Session();
        session.almUrl = url;
        session.almDomain = domain;
        session.almProject = project;

        session.clientConfig = new DefaultClientConfig();
        session.client = Client.create(session.clientConfig);

        manager = new CookieManager();
        manager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(manager);
        WebResource wr = session.client.resource(UriBuilder.fromUri(session.almUrl + "/authentication-point/login.jsp").build());
        wr = session.client.resource(UriBuilder.fromUri(session.almUrl + "/authentication-point/j_spring_security_check").build());
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("j_username", login);
        String resp = wr.get(String.class);
        String[] params = resp.substring(resp.indexOf("var publicModulus"), resp.indexOf("var prefix")).replaceAll("\\r\\n", "")
                .replace(" ", "").replace("\"", "").replace("varpublicExponent=", "")
                .replace("varpublicModulus=", "").split(";");

        String r = encryptData(
                password,
                params[0],
                params[1]
        );

        formData.add("j_password", "ENCRYPTED:" + r);
        wr.queryParams(formData).post();
    }

    private boolean logout() {
        try {
            if (session != null) {
                WebResource wr = session.client.resource(UriBuilder.fromUri(session.almUrl + "/authentication-point/logout").build());
                CookieHandler.setDefault(manager);
                ClientResponse resp = wr.get(ClientResponse.class);
                setRespCodeLogout(String.valueOf(resp.getStatus()));
                setRespTextLogout(resp.toString());
                return true;
            }
        } catch (Exception ex) {
            log.info(ex.getMessage());
            setErrorText("Ошибка метода logout: " + ex);
        }
        return false;
    }

    class Session {
        private ClientConfig clientConfig;
        private Client client;
        private String almUrl;
        private String almDomain;
        private String almProject;
    }
}
