package libs.alm;

public enum AlmParams {

    SERVER_URL("Параметр для адреса сервера HP ALM", "serverUrl"),
    DOMAIN("Параметр для домена в HP ALM", "domain"),
    PROJECT("Параметр для проекта в HP ALM", "project"),
    LOGIN("Параметр для логина в HP ALM", "login"),
    PASSWORD("Параметр для пароля в HP ALM", "password"),
    STATUS("Параметр для статуса в HP ALM", "status"),
    CYCLE_ID("Параметр для ИД ТестСета в HP ALM", "cycleId"),
    TEST_ID("Параметр для ИД Теста в HP ALM", "testId"),
    TEST_CONFIG_ID("Параметр для ИД Тест-конфига в HP ALM", "testConfigId"),
    BUILD_ID("Параметр релиза тестируемого модуля", "buildId"),
    ATTACHMENT_PATH("Параметр пути к файлам для аттача", "attachmentPath"),
    ATTACHMENT_NAME("Параметр имени файла для аттача", "attachmentName"),
    RUN_COMMENT("Параметр для комментария к запуску", "runComment"),
    ALM_STEPS("Параметр для списка шагов теста", "almSteps"),
    ATTACHMENTS("Параметр для списка вложений к тесту", "attachments");


    private final String description;
    private final String name;

    AlmParams(String description, String name) {
        this.description = description;
        this.name = name;
    }

    private String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }
}
