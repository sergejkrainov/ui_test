package libs.alm;

public final class ALMAvailableTestId {

    private ALMAvailableTestId() {

    }

    /**
     *
     * @param args - параметры консоли.
     */
    public static void main(String[] args) {

        final int zero = 0;
        final int one = 1;
        final int two = 2;
        final int three = 3;
        final int four = 4;
        final int five = 5;
        final int six = 6;
        final int seven = 7;

        try {
            if (args.length < seven) {
                System.out.println("Недостаточно входных параметров!");
                System.exit(one);
            }
            String almServerUrl = args[zero];
            String almDomain = args[one];
            String almProject = args[two];
            String almLogin = args[three];
            String almPassword = args[four];
            String almCycleId = args[five];
            String rootPath = args[six];

            String res = new Api().getComparedTestId(almServerUrl, almDomain, almProject, almLogin, almPassword,
                    Integer.parseInt(almCycleId), rootPath);
            System.out.println(res);
            System.exit(0);
        } catch (Exception ex) {
            System.out.println(ex);
            System.exit(1);
        }
    }
}
