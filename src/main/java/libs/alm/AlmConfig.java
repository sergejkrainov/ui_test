package libs.alm;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class AlmConfig {
    private final Map<AlmConfigField, String> configs = new EnumMap<>(AlmConfigField.class);

    /**
     *
     * @param filename - имя файла.
     * @return - ALM конфиг.
     */
    public static AlmConfig fromFile(String filename) {
        AlmConfig config = new AlmConfig();
        try {
            config.load(filename);
        } catch (IOException ex) {
            config = null;
        }
        return config;
    }

    AlmConfig() {

    }

    public AlmConfig(AlmConfig otherConfig) {
        Objects.requireNonNull(otherConfig);
        otherConfig.configs.forEach(configs::put);
    }

    /**
     *
     * @param field - ALM field.
     * @return - значение поля.
     */
    public final String get(AlmConfigField field) {
        Objects.requireNonNull(field);

        String result = configs.get(field);
        if (result == null) {
            throw new RuntimeException("Запрошенная настройка [" + field + "] пустая!");
        }
        return result;
    }

    /**
     *
     * @param filename - имя файла.
     * @throws IOException - тип ошибки.
     */
    public final void load(String filename) throws IOException {
        Objects.requireNonNull(filename);

        try (InputStream is = getClass().getResourceAsStream(filename)) {
            Properties properties = new Properties();
            properties.load(is);
            Arrays.stream(AlmConfigField.values())
                    .filter(f -> properties.containsKey(f.getName()))
                    .forEach(k -> configs.put(k, properties.getProperty(k.getName())));
        }
    }

    /**
     *
     * @param clazz - Class.
     */
    public final void load(Class<?> clazz) {
        Objects.requireNonNull(clazz);

        if (clazz.isAnnotationPresent(AlmTest.class)) {
            load(clazz.getAnnotation(AlmTest.class));
        }
    }

    /**
     *
     * @param method - метод.
     */
    public final void load(Method method) {
        Objects.requireNonNull(method);

        if (method.isAnnotationPresent(AlmTest.class)) {
            load(method.getAnnotation(AlmTest.class));
        }
    }

    private void load(AlmTest an) {
        assert an != null;

        set(AlmConfigField.TEST_ID, an.testId());
        set(AlmConfigField.TEST_GROUP_ID, an.groupId());
        set(AlmConfigField.DOMAIN, an.domain());
        set(AlmConfigField.PROJECT, an.project());
    }

    private void set(AlmConfigField field, String value) {
        assert field != null;

        if (value != null && !"".equals(value)) {
            configs.put(field, value);
        }
    }
}
