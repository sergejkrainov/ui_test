package libs.alm;

/**
 * AlmStep model.
 */
public class AlmStep {
    private String result;
    private String status;
    private String screenShot;

    public AlmStep(String result, String status, String screenShot) {
        this.result = result;
        this.status = status;
        this.screenShot = screenShot;
    }

    public final String getResult() {
        return this.result;
    }

    public final String getStatus() {
        return this.status;
    }

    public final String getScreenShot() {
        return this.screenShot;
    }

    public final void setResult(String result) {
        this.result = result;
    }

    public final void setStatus(String status) {
        this.status = status;
    }

    public final void setScreenShot(String screenShot) {
        this.screenShot = screenShot;
    }
}
