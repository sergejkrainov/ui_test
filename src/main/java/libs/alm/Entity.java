package libs.alm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "fields" })
@XmlRootElement(name = "Entity")
public class Entity {

    @XmlElement(name = "Fields", required = true)
    private Fields fields;
    @XmlAttribute(name = "Type", required = true)
    private String type;

    public Entity(Entity entity) {
        type = entity.getType();
        fields = new Fields(entity.getFields());
    }

    public Entity() {
    }

    /** Gets the value of the fields property.
     *
     * @return possible object is {@link Fields } */
    public final Fields getFields() {
        return fields;
    }

    /** Sets the value of the fields property.
     *
     * @param value allowed object is {@link Fields } */
    public final void setFields(Fields value) {
        this.fields = value;
    }

    /** Gets the value of the type property.
     *
     * @return possible object is {@link String } */
    public final String getType() {
        return type;
    }

    /** Sets the value of the type property.
     *
     * @param value allowed object is {@link String } */
    public final void setType(String value) {
        this.type = value;
    }

    /**
     *
     * @param fieldName - имя поля.
     * @return - значение поля.
     */
    public final String getStringFieldValue(String fieldName) {

        if (fields != null) {
            for (Field f : fields.getFieldList()) {
                if (fieldName.equals(f.getName())) {
                    if (f.getValue() != null && !f.getValue().isEmpty()) {
                        return f.getValue().get(0);
                    }
                }
            }
        }

        return null;

    }

    /**
     *
     * @param fieldName - имя поля.
     * @return - значение поля.
     */
    public final long getLongFieldValue(String fieldName) {
        try {
            return Long.parseLong(getStringFieldValue(fieldName));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public final long getId() {
        return getLongFieldValue("id");
    }

    public final String getStatus() {
        return getStringFieldValue("status");
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getType()).append(" (");

        for (Field field : getFields().getFieldList()) {
            if (sb.charAt(sb.length() - 1) != '(') {
                sb.append(", ");
            }
            sb.append(field.getName()).append(" = ");
            List<String> values = field.getValue();
            if (values == null || values.isEmpty()) {
                sb.append("null");
            } else if (values.size() == 1) {
                String val = values.get(0);
                if (val == null) {
                    sb.append("null");
                } else {
                    sb.append("\"").append(val).append("\"");
                }
            } else {
                sb.append("[");
                for (String val : values) {
                    if (val == null) {
                        sb.append("null");
                    } else {
                        sb.append("\"").append(val).append("\"");
                    }
                }
                sb.append("]");
            }
        }

        sb.append(")");

        return sb.toString();
    }

}
