package libs.alm;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Helpers class.
 */
public class Helper {

    /**
     *
     * @param rootPath - корневой путь.
     * @return - список тегов.
     */
    public final ArrayList<String> getAlmTagsFromRootPath(String rootPath) {
        ArrayList<File> fList = new ArrayList<>();
        ArrayList<String> tags = new ArrayList<>();
        boolean isScenario = false;
        new Helper().getFiles(new File(rootPath), fList, "feature");
        for (File file : fList) {
            LinkedList<String> strl = new LinkedList<>();
            try (Stream<String> lines = Files.lines(file.toPath())) {
                //получаем все строки из сценария
                lines.forEach(s -> strl.add(s));
                //если сценарий не пустой
                if (strl.size() > 0) {
                    LinkedList<String> actualStr = new LinkedList<>();
                    //пробегаемся по строкам до фразы "Сценарий:" и заполняем массив
                    for (String str : strl) {
                        if (str.toLowerCase().contains("сценарий:")) {
                            isScenario = true;
                            break;
                        }
                        actualStr.add(str);
                    }

                    //если просмотренный файл содержал фразу "Сценарий:", то считаем его нормальным. Иногда в выборку попадает хлам.
                    if (!isScenario) {
                        actualStr.clear();
                    } else {
                        //смотрим выборку и проверяем теги на корректность
                        for (String str : actualStr) {
                            //выбираем только цифровые теги
                            if (str.toLowerCase().contains("функционал:")) {
                                continue;
                            }
                            if (str.toLowerCase().contains("предыстория:")) {
                                continue;
                            }
                            if (str.toLowerCase().replace(" ", "").contains("#id")) {
                                continue;
                            }
                            if (str.toLowerCase().replace(" ", "").contains("#language")) {
                                continue;
                            }
                            String exp = "@(\\d)+";
                            Pattern p = Pattern.compile(exp);
                            Matcher m = p.matcher(str);
                            int count = 0;
                            //если цифровые теги найдены, то
                            while (m.find()) {
                                count++;
                            }
                            if (count > 1) {
                                break;
                            } else if (count == 1) {
                                m = p.matcher(str);
                                while (m.find()) {
                                    //помещаем тег в скоуп прогона
                                    tags.add(str.substring(m.start(), m.end()).replace("@", ""));
                                }
                            }
                        }
                        actualStr.clear();
                        isScenario = true;
                    }
                }

            } catch (IOException ex) {
                System.out.println(ex);
            }
        }
        return tags;
    }

    /**
     *
     * @param root - корневой файл.
     * @param fileList - список файлов.
     * @param extension - расширение файла.
     */
    public final void getFiles(File root, ArrayList<File> fileList, String extension) {
        File[] files = root.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                if (extension != null) {
                    if (getFileExtension(file).equals(extension)) {
                        fileList.add(file);
                    }
                } else {
                    fileList.add(file);
                }
            } else {
                if (file.isDirectory()) {
                    getFiles(file, fileList, extension);
                }
            }
        }
    }

    private String getFileExtension(File file) {
        String extension = "";
        String fileName = file.getAbsolutePath();
        int indx = fileName.lastIndexOf(".");
        int mp = Math.max(fileName.lastIndexOf("/"), fileName.lastIndexOf("\\"));
        if (indx > mp) {
            extension = fileName.substring(indx + 1);
        }
        return extension;
    }
}
