package libs.alm;

public enum AlmConfigField {
    SERVER("alm.server"),
    USER("alm.user"),
    PASSWORD("alm.password"),

    DOMAIN("alm.domain"),
    PROJECT("alm.project"),
    TEST_GROUP_ID("alm.test.group.id"),
    TEST_ID("alm.test.id");

    private String name;

    AlmConfigField(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "AlmConfigField." + name;
    }
}
