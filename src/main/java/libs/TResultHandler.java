package libs;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by out-vasilev-vv on 17.01.2018.
 * Используется в методе execQuery в DBLib
 * обработчик запроса
 */
public interface TResultHandler<T> {
    T handle(ResultSet resultSet) throws SQLException;
}
