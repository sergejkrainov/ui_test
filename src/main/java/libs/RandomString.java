package libs;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by out-oreshin-aa on 06.04.2018.
 */
public final class RandomString {
    public static final char[] RU_LOW_LETTERS = generateRange('а', 'я');
    public static final char[] RU_UP_LETTERS = generateRange('А', 'Я');
    public static final char[] RU_LETTERS = concat(RU_LOW_LETTERS, RU_UP_LETTERS);
    public static final char[] EN_LOW_LETTERS = generateRange('a', 'z');
    public static final char[] EN_UP_LETTERS = generateRange('A', 'Z');
    public static final char[] EN_LETTERS = concat(EN_LOW_LETTERS, EN_UP_LETTERS);
    public static final char[] NUM_LETTERS = generateRange('0', '9');
    public static final char[] RU_EN_LETTERS = concat(RU_LETTERS, EN_LETTERS);
    public static final char[] ALL_LETTERS = concat(RU_EN_LETTERS, NUM_LETTERS);

    private RandomString() {

    }

    private static final  Map<Character, char[]> FORMAT_MAPPING = new HashMap<Character, char[]>() {
        {
            put('r', RU_LOW_LETTERS);
            put('R', RU_UP_LETTERS);
            put('&', RU_LETTERS);
            put('#', NUM_LETTERS);
            put('e', EN_LOW_LETTERS);
            put('E', EN_UP_LETTERS);
            put('^', EN_LETTERS);
            put('*', ALL_LETTERS);
        }
    };

    private static final Random RND = new SecureRandom();

    private static char[] concat(char[] arr1, char[] arr2) {
        char[] result = new char[arr1.length + arr2.length];
        System.arraycopy(arr1, 0, result, 0, arr1.length);
        System.arraycopy(arr2, 0, result, arr1.length, arr2.length);
        return result;
    }

    private static char[] generateRange(char begin, char end) {
        char[] result = new char[end - begin + 1];

        for (int i = begin; i <= end; i++) {
            result[i - begin] = (char) i;
        }

        return result;
    }

    /**
     *
     * @param letters - набор символов.
     * @param length - длина.
     * @return - строка из символов.
     */
    public static String generateString(char[] letters, int length) {
        StringBuilder lettersStr = new StringBuilder();
        for (int i = 0; i < length; i++) {
            lettersStr.append(letters[RND.nextInt(letters.length)]);
        }
        return lettersStr.toString();
    }

    public static char rand(char[] chars) {
        return chars[RND.nextInt(chars.length)];
    }

    /**
     *
     * @param format - формат строки.
     * @return - необходимый формат строки.
     */
    public static String generateFormat(String format) {
        char[] chars = format.toCharArray();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < format.length(); i++) {
            if (chars[i] == '/') {
                builder.append(chars[i + 1]);
                i++;
                if (i >= format.length()) {
                    break;
                }
            }
            char[] letters = FORMAT_MAPPING.get(chars[i]);
            if (letters != null) {
                builder.append(rand(letters));
            } else {
              builder.append(chars[i]);
            }
        }
        return builder.toString();
    }

    public static String getRandomLetters(int length) {
       return generateString(RU_LETTERS, length);
    }

    public static String getRandomNumericLetters(int length) {
        return generateString(NUM_LETTERS, length);
    }
}
