package libs;

import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.rules.Verifier;
import org.junit.runners.model.MultipleFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


/**
 * Класс для получения доступа к списку ошибок из приватного поля родительского класса ErrorCollector
 * Created by out-mashnev-ma on 17.04.2017.
 */

/**
 * Created by sbt-kraynov-sa on 24.01.2018.
 */
public class TestErrorCollector extends Verifier {
    private static List<Throwable> errors = new ArrayList();

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    protected final void verify() throws Throwable {
        MultipleFailureException.assertEmpty(errors);
    }

    public final void addError(Throwable error) {
        errors.add(error);
    }

    public final <T> void checkThat(T value, Matcher<T> matcher) {
        this.checkThat("", value, matcher);
    }

    /**
     *
     * @param reason - описание ошибки.
     * @param value - проверяемое значение.
     * @param matcher - с чем сравниваем.
     * @param <T> - T.
     */
    public final <T> void checkThat(final String reason, final T value, final Matcher<T> matcher) {
        this.checkSucceeds((Callable<Object>) () -> {
            Assert.assertThat(reason, value, matcher);
            return value;
        });
    }

    /**
     *
     * @param checkDescription - описание проверки.
     * @param reason - причина ошибки.
     * @param value - проверяемое значение.
     * @param matcher - с чем сравниваем.
     * @param <T> - тип проверяемого значения.
     */
    public final <T> void checkThat(final String checkDescription, final String reason, final T value, final Matcher<T> matcher) {
        int oldErrorsSize = errors.size();
        checkThat(reason, value, matcher);

        log.info("Выполняется проверка: {}", checkDescription);

        if (errors.size() > oldErrorsSize) {
            log.info("Проверка пройдена с ошибкой");
        } else {
            log.info("Проверка пройдена успешно");
        }
    }

    /**
     *
     * @param callable - callable.
     * @param <T> - T.
     * @return - T.
     */
    public final <T> T checkSucceeds(Callable<T> callable) {
        try {
            return callable.call();
        } catch (Throwable var3) {
            this.addError(var3);
            return null;
        }
    }

    public final Boolean hasErrors() {
        return !errors.isEmpty();
    }

    public final List<Throwable> getErrors() {
        return errors;
    }
}
