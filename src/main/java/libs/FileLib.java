package libs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public final class FileLib {
    private static final Logger LOG = LoggerFactory.getLogger(FileLib.class);

    private static final int SLEEP_TIME = 500;

    private FileLib() {

    }

    /**
     * Функция записи данных в файл.
     *
     * @param fName   - путь к файлу.
     * @param data    - данные, которые необходимо записать в файл.
     * @param charset - кодировка.
     */
    public static void writeToFile(String fName, String data, String charset) throws IOException {
        LOG.info("Запись в файл \"" + fName + "\", кодировка \"" + charset + "\"");

        try {
            File f = new File(fName);

            if (f.exists()) {
                f.delete();
            }

            Thread.sleep(SLEEP_TIME);

            try (PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(fName, false), charset))) {
                BufferedWriter bwf = new BufferedWriter(pw);
                bwf.write(data);
                bwf.close();
                pw.close();
            }
        } catch (Exception e) {
            e.printStackTrace();

            LOG.error("Не удается записать в файл \"" + fName + "\"");
        }
    }

    /**
     * Функция чтения данных из файла.
     * @param fileName - путь к файлу.
     * @param charset  - кодировка.
     */
    public static String readFromFile(String fileName, String charset) {
        LOG.info("Чтение файла \"" + fileName + "\"");

        String fileContent = "";
        try (FileInputStream stream = new FileInputStream(new File(fileName))) {
            FileChannel fc = stream.getChannel();
            MappedByteBuffer bufferBytes = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            fileContent = Charset.forName(charset).decode(bufferBytes).toString();
            stream.close();
        } catch (FileNotFoundException e) {
            LOG.error("Файл не найден по пути \"" + fileName + "\"");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileContent;
    }

    /**
     * @param fPath     - путь к файлу с параметрами.
     * @param mapParams - хешмеп, куда загружать параметры.
     * @throws IOException -тип ошибки.
     */
    public static void setParamsToHashMap(String fPath, HashMap<String, String> mapParams) throws IOException {
        try (FileInputStream fstream =  new FileInputStream(fPath);
             DataInputStream in = new DataInputStream(fstream);
             BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
            String sDelimiter = new String("\t");
            String sLine;

            while ((sLine = br.readLine()) != null) {
                if (sLine.contains(sDelimiter)) {
                    List<String> arr = Arrays.asList(sLine.split(sDelimiter));
                    mapParams.put(arr.get(0), arr.get(1));

                }
            }
        }
    }

    /**
     * Функция копирования двух файлов.
     * @param fileFrom - файл, откуда копировать.
     * @param fileTo   - файл, куда копировать.
     * @param charset  - кодировка.
     * @throws IOException - тип ошибки.
     */
    public static void copyFiles(String fileFrom, String fileTo, String charset) throws IOException {
        String buff = readFromFile(fileFrom, charset);
        writeToFile(fileTo, buff, charset);

    }

    /**
     * Функция удаления файлов из директории.
     *
     * @param folderName - имя директории.
     * @throws IOException - тип ошибки.
     */
    public static void clearFolder(String folderName) throws IOException {
        LOG.info("Удаление файлов из \"" + folderName + "\"");

        File dir = new File(folderName);
        File[] files = dir.listFiles();

        if (files.length > 0) {
            for (int i = 0; i < files.length; i++) {
                LOG.info("Удаление файла \"" + files[i].getName() + "\"");
                files[i].delete();
            }
        }
    }
}
