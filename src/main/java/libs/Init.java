package libs;

import config.Shared;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public final class Init {
    private static final Logger LOG = LoggerFactory.getLogger(Init.class);

    private static final int LONG_WAIT = 60;
    private static final int FAST_WAIT = 10;
    private static final int DEFAULT_WAIT = 30;

    private Init() {

    }

    /**
     * Функция инициализации браузера.
     */
    public static void initBrowser() {
        LOG.info("Инициализация браузера");

        System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\src\\main\\libs\\exe\\IEDriverServer.exe");

        DesiredCapabilities caps = DesiredCapabilities.internetExplorer();

        caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        caps.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true);
        caps.setCapability("ignoreProtectedModeSettings", true);
        caps.setCapability("ignoreZoomSetting", true);
        caps.setCapability("nativeEvents", false);
        caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        caps.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);

        InternetExplorerOptions options = new InternetExplorerOptions(caps);

        WebDriver originalWebDriver = new InternetExplorerDriver(options);

        DelegateWebDriver webDriver = new DelegateWebDriver(originalWebDriver);
        webDriver.manage().timeouts().implicitlyWait(FAST_WAIT, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(LONG_WAIT, TimeUnit.SECONDS);
        Shared.setDelegateWebDriver(webDriver);

        WebDriverWait webDriverWait = new WebDriverWait(webDriver, DEFAULT_WAIT);
        Shared.setWebDriverWait(webDriverWait);

        Actions actions = new Actions(webDriver);
        Shared.setActions(actions);
    }
}
