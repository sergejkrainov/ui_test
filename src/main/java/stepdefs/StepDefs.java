package stepdefs;

import static testbase.TestBase.ec;

import config.Config;
import config.Shared;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.Также;
import cucumber.api.java.ru.То;
import libs.WebDriverUtil;
import pages.LoginAdministrative;

import java.util.Map;

public class StepDefs {

    /**
     * Заходим на страницу авторизации и логинимся.
     */
    @Когда("^Заходим на страницу авторизации и логинимся$")
    public final void login() {
        try {
            new LoginAdministrative().login(Config.getConf().get("acAdministrateUrl"), Config.getConf().get("iftUsername"),
                    Config.getConf().get("iftPassword"));
        } catch (Exception e) {
            ec.addError(e);
        }
    }

    /**
     * Проверяем содержимое страницы после авторизации.
     */
    @То("^Проверяем содержимое страницы после авторизации$")
    public final void checkPageAuthContent() {
        try {
            WebDriverUtil.checkElementPresent("//img[@src='/moncod2-web/css/base/images/LogoSBRF.gif']");
            WebDriverUtil.checkPageContains("Редактор вкладов");
        } catch (Exception e) {
            ec.addError(e);
        }
    }

    /**
     *
     * @param confMap - ALM_Config.
     */
    @Также("^Заполняем конфиг ALM$")
    public final void fillAlmConfig(Map<String, String> confMap) {
        try {
            Shared.setAlmConfig(confMap);
        } catch (Exception e) {
            ec.addError(e);
        }
    }
}
