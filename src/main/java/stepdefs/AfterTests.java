package stepdefs;

import static testbase.TestBase.ec;

import config.Shared;
import cucumber.api.java.After;
import libs.DelegateWebDriver;
import libs.alm.AlmConfig;
import libs.alm.AlmConfigField;
import libs.alm.Api;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import testbase.UiTestBase;

public class AfterTests {

    private static DelegateWebDriver webDriver;
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private static final String CONFIG_FILE = "/alm.properties";
    private static final String FAILED = "Failed";
    private static final String PASSED = "Passed";
    private AlmConfig baseConfig;

    /**
     * Постобработка.
     */
    @After
    public final void afterTest() throws Throwable {
        webDriver = Shared.getDelegateWebDriver();
        log.info("Закрытие браузера");
        webDriver.quit();
        new UiTestBase().killDriverAndBrowser();

        baseConfig = AlmConfig.fromFile(CONFIG_FILE);
        String status = PASSED;
        String messageError = "";

        if (ec.hasErrors()) {
            status = FAILED;
            for (Throwable err : ec.getErrors()) {
                messageError += (err.getMessage() + "\n");
            }
        }

        String answer = "";
        if (sendAlm(status)) {
            answer = "Успешно!";
        } else {
            answer = "Ошибка!!!";
        }

        log.info("Отправка HP ALM:{}", answer);

        if (status.equals(FAILED)) {
            Assert.fail(messageError);
        }
    }

    private boolean sendAlm(String status) throws Throwable {
        assert status != null;
        if (baseConfig != null) {
            AlmConfig config = new AlmConfig(baseConfig);
            return new Api().addResultsToAlm(
                    config.get(AlmConfigField.SERVER),
                    Shared.getAlmConfig().get(AlmConfigField.DOMAIN.getName()),
                    Shared.getAlmConfig().get(AlmConfigField.PROJECT.getName()),
                    config.get(AlmConfigField.USER),
                    config.get(AlmConfigField.PASSWORD),
                    Integer.parseInt(Shared.getAlmConfig().get(AlmConfigField.TEST_GROUP_ID.getName())),
                    Integer.parseInt(Shared.getAlmConfig().get(AlmConfigField.TEST_ID.getName())),
                    status);
        } else {
            log.error("Ошибка загрузки конфигурации из файла [{}].", CONFIG_FILE);
            return false;
        }
    }
}
