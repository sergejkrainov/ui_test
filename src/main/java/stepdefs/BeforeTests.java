package stepdefs;

import cucumber.api.java.Before;
import libs.DelegateWebDriver;
import libs.Init;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import testbase.UiTestBase;

public class BeforeTests {

    private static DelegateWebDriver webDriver;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Before
    public final void initTest() throws Exception {
        new UiTestBase().killDriverAndBrowser();
        Init.initBrowser();
    }
}
