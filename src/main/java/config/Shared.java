package config;

import libs.DelegateWebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public final class Shared {

    private Shared() {

    }

    private static HashMap<String, String> values = new HashMap<>();
    private static HashMap<String, DateTimeFormatter> dtFormat = new HashMap<>();
    private static Map<String, String> almConfig = new HashMap<>();

    static {
        dtFormat.put("ddMMyyyyFormatter", DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        dtFormat.put("ddMMyyFormatter", DateTimeFormatter.ofPattern("dd.MM.yy"));
        dtFormat.put("dbDateFormatter", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S"));
    }

    private static LocalDateTime today = LocalDateTime.now();

    private static DelegateWebDriver webDriver;
    private static WebDriverWait webDriverWait;
    private static Actions actions;
    private static Select select;

    public static Map<String, DateTimeFormatter> getDtFormat() {
        return dtFormat;
    }

    public static LocalDateTime getLocalDateTime() {
        return today;
    }

    public static HashMap<String, String> getValues() {
        return values;
    }

    public static DelegateWebDriver getDelegateWebDriver() {
        return webDriver;
    }

    public static void setDelegateWebDriver(DelegateWebDriver webDriver) {
        Shared.webDriver = webDriver;
    }

    public static WebDriverWait getWebDriverWait() {
        return webDriverWait;
    }

    public static void setWebDriverWait(WebDriverWait webDriverWait) {
        Shared.webDriverWait = webDriverWait;
    }

    public static Actions getActions() {
        return actions;
    }

    public static void setActions(Actions actions) {
        Shared.actions = actions;
    }

    public static Select getSelect() {
        return select;
    }

    public static Map<String, String> getAlmConfig() {
        return almConfig;
    }

    public static void setAlmConfig(Map<String, String> almConfig) {
        Shared.almConfig = almConfig;
    }

}
