package config;

import java.util.HashMap;

public final class Config {

    private Config() {

    }

    private static HashMap<String, String> confMap = new HashMap();

    static {
        confMap.put("logsPath", "src\\test\\resources\\legacyfp\\logs");
        confMap.put("stUsername", "sbt-koshel-oa");
        confMap.put("stPassword", "qweasd11");
        confMap.put("iftUsername", "sbt-avtotest");
        confMap.put("iftPassword", "avto2345^&*(");
        confMap.put("acAdministrateUrl", "http://sbt-oabs-1172:9080/moncod2-web/login.jsp");
        confMap.put("webUiRole", "dep_web_reports");
        confMap.put("acAdministrateRole", "dep_web_admrep");
        confMap.put("webUIAdminRole", "dep_web_insrep");
        confMap.put("finMonitoringRole", "dep_web_fin");
    }

    public static HashMap<String, String> getConf() {
        return confMap;
    }
}
